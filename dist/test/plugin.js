"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * @Descripttion: 测试插件的各项功能是否正常
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-16 16:56:49
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-06-12 07:49:05
 */
const assert_1 = __importDefault(require("assert"));
const src_1 = require("../src");
describe("验证插件各项功能是否正常", function () {
    it("基本状态", () => {
        class plugin extends src_1.Plugin {
        }
        assert_1.default.strictEqual(typeof plugin.description, 'string');
        assert_1.default.strictEqual(plugin.disable, false);
        assert_1.default.strictEqual(typeof plugin.priority, 'number');
        // @ts-ignore
        const p1 = new plugin(null);
        // @ts-ignore
        assert_1.default.strictEqual(p1.test(null), true);
        // @ts-ignore
        assert_1.default.strictEqual(p1.process(null), undefined);
    });
    it("处理文本");
    it("处理...");
});
