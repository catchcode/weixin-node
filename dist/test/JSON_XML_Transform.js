"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const parser_1 = require("../src/tool/parser");
const assert_1 = __importDefault(require("assert"));
describe('JSON 和 XML 转换', function () {
    it('JSON to XML', function () {
        const xml = '<xml><ToUserName>toUser</ToUserName><FromUserName>fromUser</FromUserName><CreateTime>12345678</CreateTime><MsgType>text</MsgType><Content>你好</Content></xml>';
        const json = {
            ToUserName: 'toUser',
            FromUserName: 'fromUser',
            CreateTime: '12345678',
            MsgType: 'text',
            Content: '你好'
        };
        assert_1.default.deepStrictEqual(xml, parser_1.toXML(json));
    });
    it('XML to JSON', function () {
        const xml = '<xml><ToUserName>toUser</ToUserName><FromUserName>fromUser</FromUserName><CreateTime>12345678</CreateTime><MsgType>text</MsgType><Content>你好</Content></xml>';
        const json = {
            ToUserName: 'toUser',
            FromUserName: 'fromUser',
            CreateTime: '12345678',
            MsgType: 'text',
            Content: '你好'
        };
        assert_1.default.deepStrictEqual(parser_1.toJSON(xml), json);
    });
});
