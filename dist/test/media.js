"use strict";
/**
 * @name: 自由如风
 * @access: private
 * @version: 1.0
 * @param {*}
 * @return {*}
 * @description 临时素材单元测试
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = __importDefault(require("assert"));
const fs_1 = __importDefault(require("fs"));
const index_1 = require("../src/index");
const path_1 = __importDefault(require("path"));
const Downloader_1 = require("../src/tool/Downloader");
const wx = new index_1.WX({
    token: 'codeon',
    port: 4005,
    appid: 'wxfa3f61d7c730edc9',
    secret: 'f7e8a648851ad0cdb62e2ab614d2d574'
});
describe("临时素材", function () {
    before(() => __awaiter(this, void 0, void 0, function* () {
        var _a;
        this.timeout(10 * 1000);
        yield ((_a = wx.accessToken) === null || _a === void 0 ? void 0 : _a.wait());
    }));
    it("普通文件下载", function () {
        return __awaiter(this, void 0, void 0, function* () {
            this.timeout(10 * 1000);
            try {
                fs_1.default.writeFileSync(path_1.default.resolve('test', 'get', 'plain_file.png'), yield new Downloader_1.Downloader('https://www.baidu.com/img/flexible/logo/pc/result.png').getBinary(), { encoding: 'binary' });
            }
            catch (e) {
                console.log(e);
            }
        });
    });
    it("上传、获取图片", function () {
        return __awaiter(this, void 0, void 0, function* () {
            this.timeout(10 * 1000);
            const result = yield wx.media.upload('image', fs_1.default.createReadStream(path_1.default.resolve('test', 'resource', 'image.jpg')));
            assert_1.default.strictEqual(result.type, 'image');
            assert_1.default.notStrictEqual(result.media_id, undefined);
            assert_1.default.notStrictEqual(result.created_at, undefined);
            yield wx.media.getDownloader(result.media_id).save(path_1.default.resolve('test', 'get', 'image.jpg'));
        });
    });
    it("上传、获取语音", function () {
        return __awaiter(this, void 0, void 0, function* () {
            this.timeout(10 * 1000);
            const result = yield wx.media.upload('voice', fs_1.default.createReadStream(path_1.default.resolve('test', 'resource', 'voice.mp3')));
            assert_1.default.strictEqual(result.type, 'voice');
            assert_1.default.notStrictEqual(result.media_id, undefined);
            assert_1.default.notStrictEqual(result.created_at, undefined);
            yield wx.media.getDownloader(result.media_id).save(path_1.default.resolve('test', 'get', 'voice.mp3'));
        });
    });
    it("上传、获取视频", function () {
        return __awaiter(this, void 0, void 0, function* () {
            this.timeout(10 * 1000);
            const result = yield wx.media.upload('video', fs_1.default.createReadStream(path_1.default.resolve('test', 'resource', 'video.mp4')));
            assert_1.default.strictEqual(result.type, 'video');
            assert_1.default.notStrictEqual(result.media_id, undefined);
            assert_1.default.notStrictEqual(result.created_at, undefined);
            const downloader = yield wx.media.getDownloaderOfVideo(result.media_id);
            yield downloader.save(path_1.default.resolve('test', 'get', 'video.mp4'));
        });
    });
    it("上传、获取缩略图", function () {
        return __awaiter(this, void 0, void 0, function* () {
            this.timeout(10 * 1000);
            const result = yield wx.media.upload('thumb', fs_1.default.createReadStream(path_1.default.resolve('test', 'resource', 'thumb.jpg')));
            assert_1.default.strictEqual(result.type, 'thumb');
            assert_1.default.notStrictEqual(result.thumb_media_id, undefined);
            assert_1.default.notStrictEqual(result.created_at, undefined);
            const data = yield wx.media.getDownloader(result.thumb_media_id).save(path_1.default.resolve('test', 'get', 'thumb.jpg'));
        });
    });
    it("获取失败");
});
