"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * @Descripttion: 描述
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-16 16:56:35
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-17 20:50:57
 */
const assert_1 = __importDefault(require("assert"));
const index_1 = require("../src/index");
const Imitate_1 = require("../src/tool/Imitate");
const parser_1 = require("../src/tool/parser");
const imitate = new Imitate_1.Imitate({ baseURL: 'http://localhost:4001' });
const wx = new index_1.WX({
    token: 'codeon',
    port: 4001
});
describe('被动回复用户消息', function () {
    before(() => {
        class SimpleText extends index_1.Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'SimpleText';
            }
            process() {
                return 'SimpleText';
            }
        }
        class Text extends index_1.Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'text';
            }
            process() {
                return this.reply.text('hello');
            }
        }
        class Image extends index_1.Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'image';
            }
            process() {
                return this.reply.image('MediaId');
            }
        }
        class Voice extends index_1.Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'voice';
            }
            process() {
                return this.reply.voice('MediaId');
            }
        }
        class Video extends index_1.Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'video';
            }
            process() {
                return this.reply.video({ MediaId: 'MediaId', Title: 'Title', Description: 'Description' });
            }
        }
        class Music extends index_1.Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'music';
            }
            process() {
                return this.reply.music({ Title: 'title', ThumbMediaId: 'ThumbMediaId', Description: 'Description', HQMusicUrl: 'HQMusicUrl', MusicUrl: 'MusicUrl' });
            }
        }
        class News extends index_1.Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'news';
            }
            process() {
                return this.reply.news([{ Description: 'Description', PicUrl: 'PicUrl', Title: 'Title', Url: 'Url' }]);
            }
        }
        class MoreNews extends index_1.Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'morenews';
            }
            process() {
                const arr = [];
                for (let i = 0; i < 3; i++) {
                    arr.push({ Description: 'Description', PicUrl: 'PicUrl', Title: 'Title', Url: 'Url' });
                }
                return this.reply.news(arr);
            }
        }
        wx.use(Text).use(SimpleText);
        wx.use(Image);
        wx.use(Voice);
        wx.use(Video);
        wx.use(Music);
        wx.use(News);
        wx.use(MoreNews);
    });
    it("简单文本消息", () => __awaiter(this, void 0, void 0, function* () {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = yield imitate.sendText({ Content: 'SimpleText' });
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = parser_1.toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: Date.now().toString(),
            Content: 'SimpleText',
            MsgType: 'text'
        };
        result_2.CreateTime = res_json.CreateTime;
        assert_1.default.deepStrictEqual(res_json, result_2);
    }));
    it("默认文本回复", () => __awaiter(this, void 0, void 0, function* () {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = yield imitate.sendText({ Content: 'defaultString' });
        assert_1.default.deepStrictEqual(res.data, 'success');
    }));
    it("文本消息", () => __awaiter(this, void 0, void 0, function* () {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = yield imitate.sendText({ Content: 'text' });
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = parser_1.toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: Date.now().toString(),
            Content: 'hello',
            MsgType: 'text'
        };
        result_2.CreateTime = res_json.CreateTime;
        assert_1.default.deepStrictEqual(res_json, result_2);
    }));
    it('图片消息', () => __awaiter(this, void 0, void 0, function* () {
        const res = yield imitate.sendText({ Content: 'image' });
        const res_json = parser_1.toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617424541503',
            MsgType: 'image',
            Image: { MediaId: 'MediaId' }
        };
        result_2.CreateTime = res_json.CreateTime;
        assert_1.default.deepStrictEqual(res_json, result_2);
    }));
    it('语音消息', () => __awaiter(this, void 0, void 0, function* () {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = yield imitate.sendText({ Content: 'voice' });
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = parser_1.toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617426212775',
            MsgType: 'voice',
            Voice: { MediaId: 'MediaId' }
        };
        result_2.CreateTime = res_json.CreateTime;
        assert_1.default.deepStrictEqual(res_json, result_2);
    }));
    it('视频消息', () => __awaiter(this, void 0, void 0, function* () {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = yield imitate.sendText({ Content: 'video' });
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = parser_1.toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617426212775',
            MsgType: 'video',
            Video: { MediaId: 'MediaId', Title: 'Title', Description: 'Description' }
        };
        result_2.CreateTime = res_json.CreateTime;
        assert_1.default.deepStrictEqual(res_json, result_2);
    }));
    it('音乐消息', () => __awaiter(this, void 0, void 0, function* () {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = yield imitate.sendText({ Content: 'music' });
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = parser_1.toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617444601887',
            MsgType: 'music',
            Music: {
                Title: 'title',
                ThumbMediaId: 'ThumbMediaId',
                Description: 'Description',
                HQMusicUrl: 'HQMusicUrl',
                MusicUrl: 'MusicUrl'
            }
        };
        result_2.CreateTime = res_json.CreateTime;
        assert_1.default.deepStrictEqual(res_json, result_2);
    }));
    it("单图文消息", () => __awaiter(this, void 0, void 0, function* () {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = yield imitate.sendText({ Content: 'news' });
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = parser_1.toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617445964668',
            MsgType: 'news',
            Articles: {
                item: {
                    Description: 'Description',
                    PicUrl: 'PicUrl',
                    Title: 'Title',
                    Url: 'Url'
                }
            },
            ArticleCount: '1'
        };
        result_2.CreateTime = res_json.CreateTime;
        assert_1.default.deepStrictEqual(res_json, result_2);
    }));
    it("多图文消息", () => __awaiter(this, void 0, void 0, function* () {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = yield imitate.sendText({ Content: 'morenews' });
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = parser_1.toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617445964668',
            MsgType: 'news',
            Articles: {
                item: [
                    {
                        Description: 'Description',
                        PicUrl: 'PicUrl',
                        Title: 'Title',
                        Url: 'Url'
                    },
                    {
                        Description: 'Description',
                        PicUrl: 'PicUrl',
                        Title: 'Title',
                        Url: 'Url'
                    },
                    {
                        Description: 'Description',
                        PicUrl: 'PicUrl',
                        Title: 'Title',
                        Url: 'Url'
                    }
                ]
            },
            ArticleCount: '3'
        };
        result_2.CreateTime = res_json.CreateTime;
        assert_1.default.deepStrictEqual(res_json, result_2);
    }));
});
