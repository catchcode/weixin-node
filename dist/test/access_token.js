"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = __importDefault(require("assert"));
const index_1 = require("../src/index");
describe("访问令牌", function () {
    it("成功获取", function () {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const wx = new index_1.WX({
                token: 'codeon',
                port: 4002,
                appid: 'wxfa3f61d7c730edc9',
                secret: 'f7e8a648851ad0cdb62e2ab614d2d574'
            });
            assert_1.default.strictEqual(yield ((_a = wx.accessToken) === null || _a === void 0 ? void 0 : _a.wait()), true);
        });
    });
    it("失败获取", function () {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const wx = new index_1.WX({
                token: 'codeon',
                port: 4003,
                appid: 'wx',
                secret: 'f7e8a648851ad0cdb62e2ab614d2d574'
            });
            assert_1.default.strictEqual(yield ((_a = wx.accessToken) === null || _a === void 0 ? void 0 : _a.wait()), false);
        });
    });
});
