"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const assert_1 = __importDefault(require("assert"));
const index_1 = require("../src/index");
const Imitate_1 = require("../src/tool/Imitate");
const wx = new index_1.WX({
    token: 'codeon',
    port: 4000
});
const imitate = new Imitate_1.Imitate({ baseURL: 'http://localhost:4000' });
describe('token认证', function () {
    it("成功的认证", () => {
        // 使用`codeon`签名的请求
        return imitate.cutIn(Imitate_1.CutInType.usableParams).then(res => {
            // typeof res.data == 'number'
            assert_1.default.strictEqual(res.data === 1498141000469575451, true, '认证失败');
        });
    });
    it("失败的验证", () => {
        return imitate.cutIn(Imitate_1.CutInType.disabledParams).then(res => {
            // typeof res.data == 'number'
            assert_1.default.strictEqual(res.data, false, '认证失败');
        });
    });
});
