"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sha1_1 = __importDefault(require("sha1"));
const echo_1 = require("../tool/echo");
/**
 *
 * @param obj 微信服务器传入的GET参数
 * @param token 令牌
 * @returns 验证成功返回 echoStr 失败返回 false
 */
function valid(obj, token) {
    try {
        var str = [obj.nonce, obj.timestamp, token].sort().join('');
        return sha1_1.default(str) == obj.signature ? obj.echostr.toString() : false;
    }
    catch (e) {
        return false;
    }
}
function default_1(token) {
    return function (ctx, next) {
        return __awaiter(this, void 0, void 0, function* () {
            // @ts-ignore
            const data = ctx.request.query;
            if (ctx.method == 'GET' && data.echostr !== undefined) {
                ctx.body = valid(data, token);
                echo_1.echo(`接入验证>${ctx.body}`);
            }
            else {
                yield next();
            }
        });
    };
}
exports.default = default_1;
