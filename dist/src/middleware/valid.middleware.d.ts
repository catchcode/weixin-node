import Koa from 'koa';
export default function (token: string): (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) => Promise<void>;
