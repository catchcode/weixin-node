import Koa from 'koa';
import { WX } from '..';
declare const _default: (wx: WX) => (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) => Promise<void>;
export default _default;
