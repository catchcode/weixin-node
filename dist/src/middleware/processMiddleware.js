"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const parser_1 = require("../tool/parser");
const wxRequest_1 = require("../lib/wxRequest");
const wxReply_1 = require("../lib/wxReply");
const Store_1 = require("../lib/Store");
const echo_1 = require("../tool/echo");
exports.default = (wx) => function (ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        // 只处理 XML 类型
        if (parser_1.isXML(ctx.request.body)) {
            /**
             * 在所有插件外创建对象比在插件内创建对象可能好一些。
             */
            const req = new wxRequest_1.Request(ctx.request.body); // 创建请求对象
            const reply = new wxReply_1.Reply(req.data.ToUserName, req.data.FromUserName); // 创建回复对象
            const store = new Store_1.Store(req.data.FromUserName); // 基于发送方创建的一个仓库
            // 遍历所有的插件，并创建它们
            for (const plugin of wx.plugins) {
                const _plugin = new plugin(wx, req, reply, store);
                // 1.确认插件开启            
                if (plugin.disable) {
                    continue;
                }
                // 2.确认插件处理这次请求
                if (!_plugin.test()) {
                    continue;
                }
                try {
                    // 3.开始处理
                    ctx.body = yield _plugin.process(); // 等待处理结果(可能是异步函数)
                }
                catch (e) {
                    echo_1.echo(`插件 [${plugin.name}] 抛出未处理异常`, e);
                }
                // 4.确认被处理，则后续插件不在被执行
                if (!!ctx.body) {
                    // 如果插件响应了字符串，则将其当作 文本 类型响应
                    if (typeof ctx.body == 'string') {
                        ctx.body = reply.text(ctx.body);
                    }
                    if (typeof ctx.body == 'object') {
                        ctx.body = parser_1.toXML(ctx.body);
                    }
                    else {
                        continue;
                    }
                    break;
                }
            }
            // 没有插件处理时，默认回复的内容
            if (!ctx.body) {
                ctx.body = wx.option.defaultContent;
            }
            else {
                ctx.response.type = 'text/xml';
            }
            ctx.response.status = 200;
        }
        else {
            yield next();
        }
        return Promise.resolve();
    });
};
