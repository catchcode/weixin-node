"use strict";
/*
 * @Descripttion: 主要是用户操作方面的API
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-02 22:53:44
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:33:20
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.user = void 0;
const koa_router_1 = __importDefault(require("koa-router"));
exports.user = new koa_router_1.default({ prefix: '/user' });
/** 获取用户信息 */
exports.user.get('/getUserInfo', function (ctx) {
    const { openId } = ctx.request.query;
    return ctx.wx.user.getInfo(openId);
});
/** 获取自己信息 */
exports.user.get('/getSelfInfo', function (ctx) {
    ctx.body = ctx.wx.user.getInfo(ctx.wx.option.appid);
});
/** 获取用户列表 */
exports.user.get('/getUserList', function (ctx) {
    return __awaiter(this, void 0, void 0, function* () {
        ctx.body = yield ctx.wx.user.getBatchUser();
    });
});
/** 批量获取用户信息 */
exports.user.post('/getBatchInfo', function (ctx) {
    return __awaiter(this, void 0, void 0, function* () {
        const openid_array = ctx.request.body;
        ctx.body = yield ctx.wx.user.getBatchInfo(openid_array);
    });
});
