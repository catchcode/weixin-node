"use strict";
/*
 * @Descripttion: 对外暴露的 API 接口，方便其他程序调用
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-17 09:43:11
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:09:56
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.api = void 0;
const koa_router_1 = __importDefault(require("koa-router"));
const auth_1 = __importDefault(require("../apimiddleware/auth"));
const response_1 = __importDefault(require("../apimiddleware/response"));
const ResponseError_1 = require("../lib/ResponseError");
const media_1 = require("./media");
const Material_1 = require("./Material");
const system_1 = require("./system");
const option_1 = require("./option");
const user_1 = require("./user");
/**
 * 对外暴露的 API 接口，方便其他程序调用
 * @description 调用路径前面需要加上/api
 */
exports.api = new koa_router_1.default({ prefix: '/api' });
/**
 * 所有的接口都应用这个认证中间件
 */
exports.api.use(auth_1.default);
/**
 * 格式化输出
 */
exports.api.use(response_1.default);
/** 前台将通过这个路由测试 ping */
exports.api.get('ping', '/', function (ctx, next) {
    ctx.body = '与服务器通信正常';
});
exports.api.get('/error', function (ctx) {
    throw new ResponseError_1.ResponseError('抛出错误', 123);
});
/** 永久素材管理 */
exports.api.use(Material_1.material.routes(), Material_1.material.allowedMethods());
/** 临时素材管理 */
exports.api.use(media_1.media.routes(), media_1.media.allowedMethods());
/** 系统级API */
exports.api.use(system_1.system.routes(), system_1.system.allowedMethods());
/** 参数配置 */
exports.api.use(option_1.option.routes(), option_1.option.allowedMethods());
/** 用户管理 */
exports.api.use(user_1.user.routes(), user_1.user.allowedMethods());
