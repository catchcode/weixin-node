"use strict";
/*
 * @Descripttion: 浏览器发送一个描述被执行的代码对象，服务器解析这个对象并执行
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-05 10:11:14
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:11:09
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.system = void 0;
const koa_router_1 = __importDefault(require("koa-router"));
const ResponseError_1 = require("../lib/ResponseError");
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const multer_1 = __importDefault(require("@koa/multer"));
exports.system = new koa_router_1.default({ prefix: '/system' });
// JSON 的转换器，只抓换这个对象键名为 function 的键，将其还原成一个函数
function parse(key, value) {
    if (key == 'function') {
        return eval(value);
    }
    return value;
}
/** 直接执行前端发送过来的代码 */
exports.system.post('/', function (ctx, next) {
    // 将传过来的数据转换成可被执行的对象
    const clientRequest = JSON.parse(ctx.request.body, parse);
    // 将自己的app挂载上去，使其被调用
    clientRequest.app = ctx.wx;
    // 执行，并将结果返回
    ctx.body = clientRequest.function(...clientRequest.arguments);
});
/** 获取插件的信息 */
exports.system.get('/pluginsInfo', function (ctx) {
    ctx.body = ctx.wx.plugins.map(function (e) {
        return {
            // 插件的描述
            description: e.description,
            // 插件的名字
            name: e.name,
            // 插件的开关状态
            disable: e.disable,
        };
    });
    console.log(ctx.wx.plugins[0]);
});
/** 设置插件的状态 */
exports.system.put('/setPluginState', function (ctx) {
    const { disable, name } = ctx.request.body;
    const plugin = ctx.wx.plugins.find(e => e.name == name);
    if (plugin) {
        plugin.disable = Boolean(disable);
    }
    else {
        throw new ResponseError_1.ResponseError('插件不存在', 3366);
    }
    ctx.body = '修改成功';
});
/** 添加一个插件 */
exports.system.post('/addPlugin', multer_1.default().single('script'), function (ctx) {
    const buffer = ctx.request.file.buffer;
    const file_name = ctx.request.file.originalname;
    const plugin_path = ctx.wx.option.plugin_path;
    const file_path = path_1.default.join(plugin_path, file_name);
    fs_1.default.writeFileSync(file_path, buffer);
    const plugin = require(file_path);
    ctx.wx.use(plugin);
    ctx.body = '成功添加';
});
