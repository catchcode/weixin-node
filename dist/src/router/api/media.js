"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.media = void 0;
/*
 * @Descripttion: 素材管理
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-06-25 13:22:16
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:10:47
 */
const koa_router_1 = __importDefault(require("koa-router"));
exports.media = new koa_router_1.default({ prefix: '/media' });
exports.media.get('/getMaterialcount', function (ctx) {
    return __awaiter(this, void 0, void 0, function* () {
        ctx.body = yield ctx.wx.material.getMaterialcount();
    });
});
