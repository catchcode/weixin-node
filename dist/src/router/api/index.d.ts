import Router from 'koa-router';
/**
 * 对外暴露的 API 接口，方便其他程序调用
 * @description 调用路径前面需要加上/api
 */
export declare const api: Router<any, {}>;
