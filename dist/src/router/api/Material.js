"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.material = void 0;
/*
 * @Descripttion: 永久素材管理
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-02 13:23:38
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-05 12:25:40
 */
const koa_router_1 = __importDefault(require("koa-router"));
const Tmp_1 = require("../../tool/Tmp");
const fs_1 = __importDefault(require("fs"));
const mediaFile_1 = require("../apimiddleware/mediaFile");
exports.material = new koa_router_1.default({ prefix: '/material' });
/**
 * 获取用户列表
 */
exports.material.post('/getBatchUser', function (ctx, next) {
    return ctx.wx.user.getBatchUser(ctx.request.query.next_openid);
});
/**
 * 获取素材列表
 */
exports.material.post('/batchgetMaterial', function (ctx, next) {
    const params = ctx.request.body;
    return ctx.wx.material.batchgetMaterial(params);
});
/** 获取素材[不能正常使用的接口] */
exports.material.post('/getMaterial', function (ctx, next) {
    const media_id = ctx.request.query.media_id;
    return ctx.wx.material.getMaterial(media_id);
});
/**
 * 删除素材
 */
exports.material.delete('/delMaterial', function (ctx, next) {
    const { media_id } = ctx.request.query;
    return ctx.wx.material.delMaterial(media_id);
});
/**
 * 上传素材图片或者音频
 */
exports.material.post(['/addMaterialImage', '/addMaterialVoice', '/addMaterialVideo'], mediaFile_1.mediaFile, function (ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        // 获取上传的文件
        const file = ctx.request.file;
        console.log(file);
        // 创建一个临时文件
        const tmp = new Tmp_1.Tmp({ file_name: file.originalname });
        // 将上传的文件写入到临时文件中
        fs_1.default.writeFileSync(tmp.path, file.buffer);
        // 从请求路径中判断上传的类型，可能是 image 、 voice 、 video
        const type = ctx.path.substr(ctx.path.length - 5, 5).toLocaleLowerCase();
        if (type == 'image' || type == 'voice') {
            // 将临时文件上传到微信服务器里
            var res = yield ctx.wx.material.addMaterial(tmp.path, type);
        }
        else if (type == 'video') {
            // const res = await ctx.wx.material.addMaterialVideo(tmp.path,)
        }
        // 临时文件使用完毕后删除
        tmp.destroy();
        // 返回数据
        ctx.body = res;
    });
});
exports.material.post('/addMaterialNews', function (ctx, next) {
    return ctx.wx.material.addNews(ctx.request.body);
});
