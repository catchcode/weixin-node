export declare class ResponseError extends Error {
    message: string;
    code: number;
    constructor(message: string, code: number);
    toString(): string;
    toJSON(): {
        message: string;
        code: number;
    };
}
