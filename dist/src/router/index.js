"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const koa_router_1 = __importDefault(require("koa-router"));
const index_1 = require("./api/index");
/**
 * 路由
 * 其处理优先级大于插件
 */
exports.router = new koa_router_1.default();
/**
 * 验证项目是否可以正常被访问
 */
exports.router.get('/', (ctx, next) => {
    ctx.body = '<h1">你的项目正常运行</h1>';
});
/**
 * 对外开放的API接口
 */
exports.router.use(index_1.api.routes(), index_1.api.allowedMethods());
