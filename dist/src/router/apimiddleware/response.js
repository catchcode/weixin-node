"use strict";
/*
 * @Descripttion: 处理插件的响应内容，负责检测 ResponseError 异常和格式化输出
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-06-25 11:38:14
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:10:34
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const WXAxios_1 = require("../../tool/WXAxios");
const ResponseError_1 = require("../lib/ResponseError");
function default_1(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            // 尽量使用 ctx.body 返回数据
            ctx.body = {
                msg: 'ok',
                code: 200,
                // 优先使用返回值
                data: (yield next()) || ctx.body,
            };
        }
        catch (e) {
            ctx.status = 500;
            if (e instanceof ResponseError_1.ResponseError) { // 手动在中间件中抛出错误
                ctx.body = Object.assign(Object.assign({}, e.toJSON()), { data: ctx.body });
            }
            else if (e instanceof WXAxios_1.WXResponceError) { // 微信API接口报错(主要是在 axios 的拦截器里抛出的)
                ctx.body = {
                    msg: e.message,
                    code: e.code,
                };
            }
            else if (e instanceof Error) { // Error 作为父类，应该放在最后
                ctx.body = {
                    msg: e.message,
                    code: 500,
                    data: ctx.body,
                };
            }
            else {
                console.error(e);
                ctx.body = {
                    msg: "!!服务器异常!!",
                    code: 500,
                    data: ctx.body,
                };
            }
        }
        finally {
            ctx.body = JSON.stringify(ctx.body);
        }
    });
}
exports.default = default_1;
