"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ResponseError_1 = require("../lib/ResponseError");
/**
 * 验证API请求是否合法
 * @param ctx
 * @param next
 */
function default_1(ctx, next) {
    return __awaiter(this, void 0, void 0, function* () {
        function getAuthorization() {
            const base64 = Buffer.from('weixin-nodejs' + ":" + 'weixin-nodejs').toString('base64');
            return 'Basic ' + base64;
        }
        if (ctx.get('Authorization') === getAuthorization()) {
            yield next();
        }
        else {
            ctx.status = 401;
            ctx.body = new ResponseError_1.ResponseError('认证失败', 401);
        }
    });
}
exports.default = default_1;
