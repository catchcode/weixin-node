/// <reference types="koa" />
/// <reference types="koa__multer" />
/**
 * 处理上传素材的中间件
 * @see https://blog.csdn.net/qq_42778001/article/details/104442163
 */
export declare const mediaFile: import("koa").Middleware<import("koa").DefaultState, import("koa").DefaultContext, any>;
