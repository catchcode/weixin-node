/// <reference types="koa__multer" />
import Koa from 'koa';
/**
 * 验证API请求是否合法
 * @param ctx
 * @param next
 */
export default function (ctx: any | Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function): Promise<void>;
