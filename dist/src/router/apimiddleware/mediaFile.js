"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mediaFile = void 0;
/*
 * @Descripttion: 上传素材的时候使用的中间件
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-03 12:45:03
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-03 12:58:29
 */
const multer_1 = __importDefault(require("@koa/multer"));
/**
 * 处理上传素材的中间件
 * @see https://blog.csdn.net/qq_42778001/article/details/104442163
 */
exports.mediaFile = multer_1.default().single('media');
