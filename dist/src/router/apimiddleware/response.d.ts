/// <reference types="koa__multer" />
import Koa from 'koa';
export default function (ctx: any | Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function): Promise<void>;
