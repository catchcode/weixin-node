/// <reference types="koa__multer" />
import Koa from 'koa';
export default function (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function): void;
