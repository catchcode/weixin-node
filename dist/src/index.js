"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.api = exports.router = exports.Store = exports.Plugin = exports.WX = exports.Downloader = exports.Tmp = void 0;
const koa_1 = __importDefault(require("koa"));
// 加载配置文件
const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
const koa_static_1 = __importDefault(require("koa-static"));
const koa2_cors_1 = __importDefault(require("koa2-cors"));
const path_1 = __importDefault(require("path"));
const valid_middleware_1 = __importDefault(require("./middleware/valid.middleware"));
const Plugin_1 = require("./lib/Plugin");
Object.defineProperty(exports, "Plugin", { enumerable: true, get: function () { return Plugin_1.Plugin; } });
const echo_1 = require("./tool/echo"); // 控制台输出
const autoLoadingPlug_1 = __importDefault(require("./tool/autoLoadingPlug"));
const option_1 = require("./tool/option");
const processMiddleware_1 = __importDefault(require("./middleware/processMiddleware"));
const AccessToken_1 = __importDefault(require("./lib/AccessToken"));
const Media_1 = __importDefault(require("./lib/Media"));
const Material_1 = require("./lib/Material");
const Menu_1 = require("./lib/Menu");
const Custom_1 = __importDefault(require("./lib/Custom"));
const index_1 = require("./router/index");
Object.defineProperty(exports, "router", { enumerable: true, get: function () { return index_1.router; } });
const api_1 = require("./router/api");
Object.defineProperty(exports, "api", { enumerable: true, get: function () { return api_1.api; } });
const User_1 = require("./lib/User");
const Tag_1 = require("./lib/Tag");
const Qrcode_1 = require("./lib/Qrcode");
const Store_1 = require("./lib/Store");
Object.defineProperty(exports, "Store", { enumerable: true, get: function () { return Store_1.Store; } });
const Mass_1 = require("./lib/Mass");
const Template_1 = require("./lib/Template");
var Tmp_1 = require("./tool/Tmp");
Object.defineProperty(exports, "Tmp", { enumerable: true, get: function () { return Tmp_1.Tmp; } });
var Downloader_1 = require("./tool/Downloader");
Object.defineProperty(exports, "Downloader", { enumerable: true, get: function () { return Downloader_1.Downloader; } });
class WX {
    /** 构造函数 */
    constructor(option) {
        var _a, _b;
        /** koa 实例 */
        this.koa = new koa_1.default();
        /** 插件数组 */
        this.plugins = [];
        this.codeOption = option;
        this.option = option_1.joinOption(this.codeOption);
        // 判断是否填写仓库目录
        Store_1.Store.path = (_a = this.codeOption) === null || _a === void 0 ? void 0 : _a.store_path;
        // 将自己挂载到koa实例上
        this.koa.context.wx = this;
        // 判断是否开启访问令牌模块
        if (this.option.appid && this.option.secret) {
            this.accessToken = new AccessToken_1.default(this.option.appid, this.option.secret);
        }
        // 判断是否填写插件目录
        autoLoadingPlug_1.default((_b = this.codeOption) === null || _b === void 0 ? void 0 : _b.plugin_path, this);
        /** 临时素材管理 */
        this.media = new Media_1.default(this);
        /** 永久素材管理 */
        this.material = new Material_1.Material(this);
        /** 自定义菜单 */
        this.menu = new Menu_1.Menu(this);
        /** 客服 */
        this.custom = new Custom_1.default(this);
        /** 用户管理 */
        this.user = new User_1.User(this);
        /** 标签管理 */
        this.tag = new Tag_1.Tag(this);
        /** 二维码工具 */
        this.qrcode = new Qrcode_1.Qrcode(this);
        /** 群发 */
        this.mass = new Mass_1.Mass(this);
        /** 模板消息 */
        this.template = new Template_1.Template(this);
        /** 允许跨域 */
        this.koa.use(koa2_cors_1.default());
        /** 静态资源服务器 */
        this.koa.use(koa_static_1.default(path_1.default.join(__dirname, '../', 'public')));
        // 解析请求体
        this.koa.use(koa_bodyparser_1.default({
            enableTypes: ['json', 'xml', 'form', 'text'],
            formLimit: '10mb',
            extendTypes: {
                text: ['application/javascript'] // will parse application/x-javascript type body as a JSON string
            }
        }));
        // 请求/响应日志
        this.koa.use((ctx, next) => __awaiter(this, void 0, void 0, function* () {
            const start_time = Date.now();
            echo_1.echo(`>>> [${ctx.request.path}] ${JSON.stringify(ctx.request.body)}`.substr(0, 200));
            yield next();
            echo_1.echo(`<<< [${Date.now() - start_time}ms][${ctx.status}] ${ctx.body} `.substr(0, 200));
        }));
        // 添加接入认证插件(只处理 GET 且携带 echostr 参数)
        this.koa.use(valid_middleware_1.default(this.option.token));
        // 路由
        this.koa.use(index_1.router.routes());
        // 插件处理机制
        this.koa.use(processMiddleware_1.default(this));
        // 启动
        this.koa.listen(this.option.port).address();
        echo_1.echo(`服务已启动和运行,地址是:http://localhost:${this.option.port}`);
    }
    /** 访问令牌（字符串） */
    get access_token() { var _a, _b; return (_b = (_a = this.accessToken) === null || _a === void 0 ? void 0 : _a.toString()) !== null && _b !== void 0 ? _b : ''; }
    /**
     * 添加这个插件到插件列表
     * @param plugin 插件类 或者 一个由插件类组成的数组
     * @returns 返回自己
     */
    use(plugin) {
        if (plugin instanceof Array) {
            // 触发使用事件
            plugin.forEach(e => e.onUse(this));
            this.plugins = this.plugins.concat(plugin);
        }
        else {
            // 触发使用事件
            plugin.onUse(this);
            this.plugins.push(plugin);
        }
        // 对插件进行排序
        this.sort_public();
        return this;
    }
    /** 对插件进行排序，按优先级 */
    sort_public() {
        // 这里采用的降序排序，如果升序则使用 p1 - p2
        this.plugins.sort((p1, p2) => p2.priority - p1.priority);
    }
}
exports.WX = WX;
