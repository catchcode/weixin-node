/// <reference types="node" />
/// <reference types="got" />
export declare class Downloader {
    private download;
    /**
     *
     * @param url 下载文件的路径，可以传string或者URL实例
     */
    constructor(url: string | URL);
    /**
     * 获取文件的二进制数据
     * @returns
     */
    getBinary(): Promise<Buffer> & import("got").GotEmitter & import("stream").Duplex;
    /**
     * 将文件保存到文件
     * @param file 保存的文件路径
     * @returns
     */
    save(file: string | URL): Promise<void>;
}
