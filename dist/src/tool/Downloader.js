"use strict";
/*
 * @Descripttion: 下载器
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-06-12 06:58:06
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-02 17:03:59
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Downloader = void 0;
const download_1 = __importDefault(require("download"));
const fs_1 = __importDefault(require("fs"));
class Downloader {
    /**
     *
     * @param url 下载文件的路径，可以传string或者URL实例
     */
    constructor(url) {
        this.download = download_1.default(url.toString());
    }
    /**
     * 获取文件的二进制数据
     * @returns
     */
    getBinary() {
        return this.download;
    }
    /**
     * 将文件保存到文件
     * @param file 保存的文件路径
     * @returns
     */
    save(file) {
        return __awaiter(this, void 0, void 0, function* () {
            return fs_1.default.writeFileSync(file, yield this.download);
        });
    }
}
exports.Downloader = Downloader;
