/** 将XML字符串转换成 JS 对象 */
export declare function toJSON(xml_str: string): any;
export declare function isXML(xml_str: string): boolean;
/** 将 JSON 或者 JS对象转换成 XML 字符串 */
export declare function toXML(json_or_object: object): string;
