"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.joinOption = exports.storeOption = exports.defaultOption = void 0;
/*
 * @Descripttion: 创建WX实例需要的参数
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-12 10:12:56
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 13:22:38
 */
const path_1 = __importDefault(require("path"));
/**
 * 默认配置
 */
exports.defaultOption = {
    port: 80,
    access_token_interval: 7000,
    defaultContent: 'success',
    /** 默认的插件目录 */
    plugin_path: path_1.default.join(process.cwd(), 'plugin'),
    /** 默认的仓库目录 */
    store_path: path_1.default.join(process.cwd(), 'store')
};
// 这个仓库必须在 defaultOption 下面引入，因为它里面使用了上面的配置
const Store_1 = require("../lib/Store");
/** 仓库配置 */
exports.storeOption = new Store_1.Store('env');
/**
 * @name: 自由如风
 * @access: private
 * @version: 1.0
 * @param {Option} option
 * @return {Option}
 * @description 合并配置,这是使用的理念的是 代码配置 > UI 配置 > 默认配置
 */
function joinOption(option) {
    return Object.assign(Object.assign(Object.assign({}, exports.defaultOption), exports.storeOption.getAll()), option);
}
exports.joinOption = joinOption;
