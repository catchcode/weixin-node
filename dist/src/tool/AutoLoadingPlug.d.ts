import { WX } from '..';
export default function (path: string | undefined, wx: WX): Promise<void>;
