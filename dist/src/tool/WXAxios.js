"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.create_axios = exports.WXResponceError = void 0;
const axios_1 = __importDefault(require("axios"));
class WXResponceError extends Error {
    constructor(message, code) {
        super();
        this.message = message;
        this.code = code;
    }
}
exports.WXResponceError = WXResponceError;
/**
 * 创建一个适用于微信公众号的axios实例微信实例
 * @param wx 微信实例
 * @returns
 */
function create_axios(wx, config) {
    const axios = axios_1.default.create(config);
    axios.interceptors.request.use(function (request) {
        if (!!(wx === null || wx === void 0 ? void 0 : wx.access_token)) {
            request.params = Object.assign({ access_token: wx === null || wx === void 0 ? void 0 : wx.access_token }, request.params);
        }
        return request;
    });
    axios.interceptors.response.use(function (response) {
        // 拥有errcode 参数 ，且不为 0（成功）
        if (!!response.data.errcode && response.data.errcode != 0) {
            const e = response.data;
            throw new WXResponceError(e.errmsg, e.errcode); // 抛出异常
        }
        return response.data; // 
    });
    return axios;
}
exports.create_axios = create_axios;
