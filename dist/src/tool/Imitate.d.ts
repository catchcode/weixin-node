import { AxiosInstance, AxiosRequestConfig } from 'axios';
import { Any as PushAny, Text, Image, Voice, Video, ShortVideo, Location, Link } from '../lib/wxRequest';
export declare enum CutInType {
    /** 可以认证成功的请求参数 */
    usableParams = "/?signature=f43587d7f5a37254502b642491db6c18f54458dd&echostr=1498141000469575451&timestamp=1617360658&nonce=1743689942",
    /** 不能认证成功的请求参数 */
    disabledParams = "/?signature=43587d7f5a37254502b642491db6c18f54458dd&echostr=1498141000469575451&timestamp=1617360658&nonce=1743689942"
}
/**
 * 模拟微信服务器向自己发送请求（单元测试时使用）
 */
export declare class Imitate {
    axios: AxiosInstance;
    constructor({ ...args }: AxiosRequestConfig);
    cutIn(url?: string): Promise<import("axios").AxiosResponse<any>>;
    /**
     * 模拟 微信服务器发送请求
     * @param params 需要发送的内容，满足Push接口要求
     * @returns
     */
    send(params: PushAny): Promise<any>;
    sendText(text: Text): Promise<any>;
    sendImage(image: Image): Promise<any>;
    sendVoice(voice: Voice): Promise<any>;
    sendVideo(video: Video): Promise<any>;
    sendShortVideo(shortvideo: ShortVideo): Promise<any>;
    sendLocation(location: Location): Promise<any>;
    sendlink(link: Link): Promise<any>;
}
