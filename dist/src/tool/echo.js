"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.echo = void 0;
/** 打印控制台日志 */
function echo(...message) {
    var _a;
    /** 单元测试时不输出 */
    if (((_a = process.env.NODE_ENV) === null || _a === void 0 ? void 0 : _a.trim()) == 'test') {
        return;
    }
    console.log(`[${new Date().toLocaleTimeString()}]`, ...message, '\n');
}
exports.echo = echo;
