"use strict";
/*
 * @Descripttion: JSON 和 XML 相互转换
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-13 15:40:53
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-04-13 17:26:53
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.toXML = exports.isXML = exports.toJSON = void 0;
const fast_xml_parser_1 = __importDefault(require("fast-xml-parser"));
/** 将XML字符串转换成 JS 对象 */
function toJSON(xml_str) {
    var res = fast_xml_parser_1.default.parse(xml_str, { parseNodeValue: false }).xml;
    return res;
}
exports.toJSON = toJSON;
function isXML(xml_str) {
    return fast_xml_parser_1.default.validate(xml_str) === true;
}
exports.isXML = isXML;
const j2xParser = new fast_xml_parser_1.default.j2xParser({});
/** 将 JSON 或者 JS对象转换成 XML 字符串 */
function toXML(json_or_object) {
    return j2xParser.parse({ xml: json_or_object });
}
exports.toXML = toXML;
