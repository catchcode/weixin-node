export interface InterfaceOption {
    token: string;
    /** 开发者ID  */
    appid?: string;
    /** 开发者密码(AppSecret)  */
    secret?: string;
    /** 监听的端口 */
    port?: number;
    /** 自动刷新 access token 的时间间隔,单位 秒 ,默认 7000*/
    access_token_interval?: number;
    /** 当没有插件处理的时候回复的内容，默认 success  */
    defaultContent?: string;
    /** 插件的目录 */
    plugin_path?: string;
    /** 仓库的目录 */
    store_path?: string;
}
/**
 * 默认配置
 */
export declare const defaultOption: {
    port: number;
    access_token_interval: number;
    defaultContent: string;
    /** 默认的插件目录 */
    plugin_path: string;
    /** 默认的仓库目录 */
    store_path: string;
};
import { Store } from '../lib/Store';
/** 仓库配置 */
export declare const storeOption: Store;
/**
 * @name: 自由如风
 * @access: private
 * @version: 1.0
 * @param {Option} option
 * @return {Option}
 * @description 合并配置,这是使用的理念的是 代码配置 > UI 配置 > 默认配置
 */
export declare function joinOption(option: InterfaceOption): InterfaceOption;
