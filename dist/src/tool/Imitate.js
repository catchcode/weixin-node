"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Imitate = exports.CutInType = void 0;
/*
 * @Descripttion: 模拟微信服务器推送消息时间，主要应用于单元测试里
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-13 19:00:05
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-16 15:44:15
 */
const axios_1 = __importDefault(require("axios"));
const parser_1 = require("./parser");
var CutInType;
(function (CutInType) {
    /** 可以认证成功的请求参数 */
    CutInType["usableParams"] = "/?signature=f43587d7f5a37254502b642491db6c18f54458dd&echostr=1498141000469575451&timestamp=1617360658&nonce=1743689942";
    /** 不能认证成功的请求参数 */
    CutInType["disabledParams"] = "/?signature=43587d7f5a37254502b642491db6c18f54458dd&echostr=1498141000469575451&timestamp=1617360658&nonce=1743689942";
})(CutInType = exports.CutInType || (exports.CutInType = {}));
/**
 * 模拟微信服务器向自己发送请求（单元测试时使用）
 */
class Imitate {
    constructor(_a) {
        var args = __rest(_a, []);
        this.axios = axios_1.default.create(Object.assign({ 
            // 默认的请求头
            headers: {
                'user-agent': 'Mozilla/4.0',
                accept: '*/*',
                'Content-Type': 'text/xml',
                pragma: 'no-cache',
                connection: 'Keep-Alive',
            } }, args));
    }
    cutIn(url = CutInType.usableParams || CutInType.disabledParams) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.get(url);
        });
    }
    /**
     * 模拟 微信服务器发送请求
     * @param params 需要发送的内容，满足Push接口要求
     * @returns
     */
    send(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post('', parser_1.toXML(params).toString()).catch(e => {
                console.log(e.message);
            });
        });
    }
    sendText(text) {
        var _a, _b, _c, _d;
        return __awaiter(this, void 0, void 0, function* () {
            return this.send({
                ToUserName: (_a = text.ToUserName) !== null && _a !== void 0 ? _a : 'ToUserName',
                FromUserName: (_b = text.FromUserName) !== null && _b !== void 0 ? _b : 'FromUserName',
                MsgType: 'text',
                CreateTime: Date.now().toString(),
                MsgId: (_c = text === null || text === void 0 ? void 0 : text.MsgId) !== null && _c !== void 0 ? _c : 'MsgId',
                Content: (_d = text === null || text === void 0 ? void 0 : text.Content) !== null && _d !== void 0 ? _d : 'Content',
            });
        });
    }
    sendImage(image) {
        var _a, _b, _c, _d, _e;
        return __awaiter(this, void 0, void 0, function* () {
            return this.send({
                ToUserName: (_a = image.ToUserName) !== null && _a !== void 0 ? _a : 'ToUserName',
                FromUserName: (_b = image.FromUserName) !== null && _b !== void 0 ? _b : 'FromUserName',
                MsgType: 'image',
                CreateTime: Date.now().toString(),
                MsgId: (_c = image.MsgId) !== null && _c !== void 0 ? _c : 'MsgId',
                PicUrl: (_d = image.PicUrl) !== null && _d !== void 0 ? _d : 'PicUrl',
                MediaId: (_e = image.MediaId) !== null && _e !== void 0 ? _e : 'MediaId'
            });
        });
    }
    sendVoice(voice) {
        var _a, _b, _c, _d, _e, _f;
        return __awaiter(this, void 0, void 0, function* () {
            return this.send({
                ToUserName: (_a = voice.ToUserName) !== null && _a !== void 0 ? _a : 'ToUserName',
                FromUserName: (_b = voice.FromUserName) !== null && _b !== void 0 ? _b : 'FromUserName',
                MsgType: 'voice',
                CreateTime: Date.now().toString(),
                MsgId: (_c = voice.MsgId) !== null && _c !== void 0 ? _c : 'MsgId',
                Format: (_d = voice.Format) !== null && _d !== void 0 ? _d : 'Format',
                Recognition: (_e = voice.Recognition) !== null && _e !== void 0 ? _e : 'Recognition',
                MediaId: (_f = voice.MediaId) !== null && _f !== void 0 ? _f : 'MediaId'
            });
        });
    }
    sendVideo(video) {
        var _a, _b, _c, _d, _e;
        return __awaiter(this, void 0, void 0, function* () {
            return this.send({
                ToUserName: (_a = video.ToUserName) !== null && _a !== void 0 ? _a : 'ToUserName',
                FromUserName: (_b = video.FromUserName) !== null && _b !== void 0 ? _b : 'FromUserName',
                MsgType: 'video',
                CreateTime: Date.now().toString(),
                MsgId: (_c = video.MsgId) !== null && _c !== void 0 ? _c : 'MsgId',
                ThumbMediaId: (_d = video.ThumbMediaId) !== null && _d !== void 0 ? _d : 'ThumbMediaId',
                MediaId: (_e = video.MediaId) !== null && _e !== void 0 ? _e : 'MediaId'
            });
        });
    }
    sendShortVideo(shortvideo) {
        var _a, _b, _c, _d, _e;
        return __awaiter(this, void 0, void 0, function* () {
            return this.send({
                ToUserName: (_a = shortvideo.ToUserName) !== null && _a !== void 0 ? _a : 'ToUserName',
                FromUserName: (_b = shortvideo.FromUserName) !== null && _b !== void 0 ? _b : 'FromUserName',
                MsgType: 'shortvideo',
                CreateTime: Date.now().toString(),
                MsgId: (_c = shortvideo.MsgId) !== null && _c !== void 0 ? _c : 'MsgId',
                ThumbMediaId: (_d = shortvideo.ThumbMediaId) !== null && _d !== void 0 ? _d : 'ThumbMediaId',
                MediaId: (_e = shortvideo.MediaId) !== null && _e !== void 0 ? _e : 'MediaId'
            });
        });
    }
    sendLocation(location) {
        var _a, _b, _c, _d, _e, _f, _g;
        return __awaiter(this, void 0, void 0, function* () {
            return this.send({
                ToUserName: (_a = location.ToUserName) !== null && _a !== void 0 ? _a : 'ToUserName',
                FromUserName: (_b = location.FromUserName) !== null && _b !== void 0 ? _b : 'FromUserName',
                MsgType: 'location',
                CreateTime: Date.now().toString(),
                MsgId: (_c = location.MsgId) !== null && _c !== void 0 ? _c : 'MsgId',
                Location_X: (_d = location.Location_X) !== null && _d !== void 0 ? _d : 'Location_X',
                Location_Y: (_e = location.Location_Y) !== null && _e !== void 0 ? _e : 'Location_Y',
                Scale: (_f = location.Scale) !== null && _f !== void 0 ? _f : 'Scale',
                Label: (_g = location.Label) !== null && _g !== void 0 ? _g : 'Label',
            });
        });
    }
    sendlink(link) {
        var _a, _b, _c, _d, _e, _f;
        return __awaiter(this, void 0, void 0, function* () {
            return this.send({
                ToUserName: (_a = link.ToUserName) !== null && _a !== void 0 ? _a : 'ToUserName',
                FromUserName: (_b = link.FromUserName) !== null && _b !== void 0 ? _b : 'FromUserName',
                MsgType: 'link',
                CreateTime: Date.now().toString(),
                MsgId: (_c = link.MsgId) !== null && _c !== void 0 ? _c : 'MsgId',
                Title: (_d = link.Title) !== null && _d !== void 0 ? _d : 'Title',
                Description: (_e = link.Description) !== null && _e !== void 0 ? _e : 'Title',
                Url: (_f = link.Url) !== null && _f !== void 0 ? _f : 'Title',
            });
        });
    }
}
exports.Imitate = Imitate;
