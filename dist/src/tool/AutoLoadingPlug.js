"use strict";
/*
 * @Descripttion: 自动加载插件
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-16 12:29:09
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-05 19:42:53
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const echo_1 = require("./echo");
const path_1 = __importDefault(require("path"));
const option_1 = require("./option");
function default_1(path = option_1.defaultOption.plugin_path, wx) {
    var e_1, _a;
    var _b;
    return __awaiter(this, void 0, void 0, function* () {
        // 等待 accesstoken 获取成功之后再加载插件
        yield ((_b = wx.accessToken) === null || _b === void 0 ? void 0 : _b.wait());
        // 判断目录是否存在，不存在则创建
        if (!fs_1.default.existsSync(path)) {
            fs_1.default.mkdirSync(path);
            echo_1.echo(`插件目录不存在，已创建[${path}]`);
        }
        // 打开这个目录
        const dirs = yield fs_1.default.promises.opendir(path).catch(e => {
            console.error("打开插件目录失败，请检查 plugin_path 参数对应的目录是否正常。");
            process.exit();
        });
        try {
            for (var dirs_1 = __asyncValues(dirs), dirs_1_1; dirs_1_1 = yield dirs_1.next(), !dirs_1_1.done;) {
                const dir = dirs_1_1.value;
                try {
                    // 不加载 非js 文件
                    if (!/\.js/.test(dir.name)) {
                        continue;
                    }
                    const plugin = require(path_1.default.join(path, dir.name));
                    wx.use(plugin);
                    echo_1.echo(`加载插件[${plugin.name}]成功`);
                }
                catch (e) {
                    echo_1.echo(`自动加载插件失败,文件名：${dir.name}`);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (dirs_1_1 && !dirs_1_1.done && (_a = dirs_1.return)) yield _a.call(dirs_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
    });
}
exports.default = default_1;
