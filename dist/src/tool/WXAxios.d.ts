import { AxiosRequestConfig } from 'axios';
import { WX } from '..';
export declare class WXResponceError extends Error {
    message: string;
    code: number;
    constructor(message: string, code: number);
}
/**
 * 创建一个适用于微信公众号的axios实例微信实例
 * @param wx 微信实例
 * @returns
 */
export declare function create_axios(wx?: WX, config?: AxiosRequestConfig): import("axios").AxiosInstance;
