"use strict";
/*
 * @Descripttion: 创建一个临时文件
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-02 16:09:09
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-03 14:04:12
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tmp = void 0;
const os_1 = __importDefault(require("os"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
/**
 * 创建一个临时文件
 */
class Tmp {
    /**
     * 创建一个临时文件，如果你要销毁临时文件，请使用 disroy 方法销毁
     * 如果你在创建临时文件的时候指定了 file_name 参数，那么 prefile 参数将失效
     * 如果你指定的 file_name 已经存在，那么删除之前的文件
     * @param params 参数
     */
    constructor(params) {
        var _a;
        if (params.file_name) {
            this.path = path_1.default.join(Tmp.DIR_PATH, params.file_name);
            // 如果之前存在，则删除之前的文件
            if (fs_1.default.existsSync(this.path)) {
                fs_1.default.rmSync(this.path);
            }
            const f = fs_1.default.openSync(this.path, 'w');
            fs_1.default.closeSync(f);
        }
        else {
            this.path = Tmp.randName() + ((_a = params.prefix) !== null && _a !== void 0 ? _a : '');
        }
    }
    static randName() {
        return Date.now().toString() + Math.random().toString();
    }
    /** 销毁临时文件 */
    destroy() {
        return fs_1.default.rmSync(this.path);
    }
    /** 重命名文件 */
    rename(new_name) {
        /** 之前的名字 */
        const old_path = this.path;
        /** 新的名字 */
        const new_path = path_1.default.join(old_path, '..', new_name);
        // 重命名
        fs_1.default.renameSync(old_path, new_path);
        // 更换自己的路径
        this.path = new_path;
    }
}
exports.Tmp = Tmp;
/** 临时文件保存的路径 */
Tmp.DIR_PATH = path_1.default.join(os_1.default.tmpdir(), 'weixin-nodejs');
// 如果目录不存在 则创建这个目录
if (!fs_1.default.existsSync(Tmp.DIR_PATH)) {
    fs_1.default.mkdirSync(Tmp.DIR_PATH);
}
