/**
 * 创建一个临时文件
 */
export declare class Tmp {
    /** 临时文件保存的路径 */
    static DIR_PATH: string;
    static randName(): string;
    /** 临时文件的路径 */
    path: string;
    /**
     * 创建一个临时文件，如果你要销毁临时文件，请使用 disroy 方法销毁
     * 如果你在创建临时文件的时候指定了 file_name 参数，那么 prefile 参数将失效
     * 如果你指定的 file_name 已经存在，那么删除之前的文件
     * @param params 参数
     */
    constructor(params: {
        /** 文件名 */
        file_name?: string;
        /** 后缀 */
        prefix?: string;
    });
    /** 销毁临时文件 */
    destroy(): void;
    /** 重命名文件 */
    rename(new_name: string): void;
}
