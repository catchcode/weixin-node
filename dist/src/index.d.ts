/// <reference types="koa-bodyparser" />
/// <reference types="koa__multer" />
import Koa from 'koa';
import { Plugin, PublicClass } from './lib/Plugin';
import { InterfaceOption } from './tool/option';
import AccessToken from './lib/AccessToken';
import Media from './lib/Media';
import { Material } from './lib/Material';
import { Menu } from './lib/Menu';
import Custom from './lib/Custom';
import { router } from './router/index';
import { api } from './router/api';
import { User } from './lib/User';
import { Tag } from './lib/Tag';
import { Qrcode } from './lib/Qrcode';
import { Store } from './lib/Store';
import { Mass } from './lib/Mass';
import { Template } from './lib/Template';
export { Tmp } from './tool/Tmp';
export { Downloader } from './tool/Downloader';
declare class WX {
    /** koa 实例 */
    koa: Koa;
    /** 访问令牌的实例（对象） */
    accessToken: AccessToken | undefined;
    /** 访问令牌（字符串） */
    get access_token(): string;
    /** 临时素材 */
    media: Media;
    /** 永久素材 */
    material: Material;
    /** 自定义菜单 */
    menu: Menu;
    /** 客服 */
    custom: Custom;
    /** 用户管理 */
    user: User;
    /** 标签管理 */
    tag: Tag;
    /** 二维码工具 */
    qrcode: Qrcode;
    /** 群发 */
    mass: Mass;
    /** 模板消息 */
    template: Template;
    /** 插件数组 */
    plugins: PublicClass[];
    /** 构造函数参数，被使用的参数 */
    option: InterfaceOption;
    /** 构造函数参数，通过参数传入的原始数据 */
    readonly codeOption: InterfaceOption;
    /** 构造函数 */
    constructor(option: InterfaceOption);
    /**
     * 添加这个插件到插件列表
     * @param plugin 插件类 或者 一个由插件类组成的数组
     * @returns 返回自己
     */
    use(plugin: PublicClass | PublicClass[]): WX;
    /** 对插件进行排序，按优先级 */
    private sort_public;
}
export { WX, Plugin, Store, router, api };
