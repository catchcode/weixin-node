"use strict";
/*
 * @Descripttion: 客服消息
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 08:39:28
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-06-27 13:12:23
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const WXAxios_1 = require("../tool/WXAxios");
class Custom {
    constructor(wx) {
        this.SendURL = 'https://api.weixin.qq.com/cgi-bin/message/custom/send';
        this.TypingURL = ' https://api.weixin.qq.com/cgi-bin/message/custom/typing';
        this.app = wx;
        this.axios = WXAxios_1.create_axios(wx);
    }
    /** 客服接口-发消息 */
    send(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield this.axios.post(this.SendURL, params));
        });
    }
    /** 客服接口-发送文本消息 */
    sendText(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.send(Object.assign(Object.assign({}, params), { msgtype: 'text' }));
        });
    }
    /** 客服接口-发送图片消息 */
    sendImage(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.send(Object.assign(Object.assign({}, params), { msgtype: 'image' }));
        });
    }
    /** 客服接口-发送语音消息 */
    sendVoice(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.send(Object.assign(Object.assign({}, params), { msgtype: 'voice' }));
        });
    }
    /** 客服接口-发送语音消息 */
    sendVideo(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.send(Object.assign(Object.assign({}, params), { msgtype: 'video' }));
        });
    }
    /** 客服接口-发送音乐消息 */
    sendMusic(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.send(Object.assign(Object.assign({}, params), { msgtype: 'music' }));
        });
    }
    /** 客服接口-发送图文消息（点击跳转到外链） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。 */
    sendNews(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.send(Object.assign(Object.assign({}, params), { msgtype: 'news' }));
        });
    }
    /** 客服接口-发送图文消息（点击跳转到外链） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。 */
    sendNewsByMediaId(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.send(Object.assign(Object.assign({}, params), { msgtype: 'mpnews' }));
        });
    }
    /** 客服接口-发送菜单消息 */
    sendMsgMenu(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.send(Object.assign(Object.assign({}, params), { msgtype: 'msgmenu' }));
        });
    }
    /** 客服输入状态 */
    typing(OPENID) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.TypingURL, { touser: OPENID, "command": "Typing" });
        });
    }
}
exports.default = Custom;
