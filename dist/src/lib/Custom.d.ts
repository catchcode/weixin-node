import { WX } from '../index';
declare type Msgtypes = 'text' | 'image' | 'voice' | 'video' | 'music' | 'news' | 'mpnews' | 'msgmenu';
declare class Custom {
    private axios;
    private app;
    private readonly SendURL;
    private readonly TypingURL;
    constructor(wx: WX);
    /** 客服接口-发消息 */
    send(params: {
        msgtype: Msgtypes;
    }): Promise<{
        errcode: 0;
        errmsg: "ok";
    }>;
    /** 客服接口-发送文本消息 */
    sendText(params: {
        /** 普通用户openid */
        touser: string;
        text: {
            /** 文本消息内容 */
            content: string;
        };
    }): Promise<{
        errcode: 0;
        errmsg: "ok";
    }>;
    /** 客服接口-发送图片消息 */
    sendImage(params: {
        /** 普通用户openid */
        touser: string;
        image: {
            /** 图片的素材ID */
            media_id: string;
        };
    }): Promise<{
        errcode: 0;
        errmsg: "ok";
    }>;
    /** 客服接口-发送语音消息 */
    sendVoice(params: {
        /** 普通用户openid */
        touser: string;
        voice: {
            /** 语音的素材ID */
            media_id: string;
        };
    }): Promise<{
        errcode: 0;
        errmsg: "ok";
    }>;
    /** 客服接口-发送语音消息 */
    sendVideo(params: {
        /** 普通用户openid */
        touser: string;
        video: {
            media_id: string;
            thumb_media_id: string;
            title: string;
            description: string;
        };
    }): Promise<{
        errcode: 0;
        errmsg: "ok";
    }>;
    /** 客服接口-发送音乐消息 */
    sendMusic(params: {
        /** 普通用户openid */
        touser: string;
        music: {
            title: string;
            description: string;
            musicurl: string;
            hqmusicurl: string;
            thumb_media_id: string;
        };
    }): Promise<{
        errcode: 0;
        errmsg: "ok";
    }>;
    /** 客服接口-发送图文消息（点击跳转到外链） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。 */
    sendNews(params: {
        /** 普通用户openid */
        touser: string;
        news: {
            articles: [
                {
                    title: string;
                    description: string;
                    url: string;
                    picurl: string;
                }
            ];
        };
    }): Promise<{
        errcode: 0;
        errmsg: "ok";
    }>;
    /** 客服接口-发送图文消息（点击跳转到外链） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。 */
    sendNewsByMediaId(params: {
        /** 普通用户openid */
        touser: string;
        mpnews: {
            media_id: string;
        };
    }): Promise<{
        errcode: 0;
        errmsg: "ok";
    }>;
    /** 客服接口-发送菜单消息 */
    sendMsgMenu(params: {
        /** 普通用户openid */
        touser: string;
        msgmenu: {
            head_content: string;
            list: [
                {
                    id: number;
                    content: string;
                }
            ];
            tail_content: string;
        };
    }): Promise<{
        errcode: 0;
        errmsg: "ok";
    }>;
    /** 客服输入状态 */
    typing(OPENID: string): Promise<import("axios").AxiosResponse<any>>;
}
export default Custom;
