"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Qrcode = void 0;
const Downloader_1 = require("../tool/Downloader");
const WXAxios_1 = require("../tool/WXAxios");
class Qrcode {
    constructor(wx) {
        this.wx = wx;
        this.CREATE = 'https://api.weixin.qq.com/cgi-bin/qrcode/create';
        this.SHOW_QRCODE = 'https://mp.weixin.qq.com/cgi-bin/showqrcode';
        this.app = wx;
        this.axios = WXAxios_1.create_axios(wx);
    }
    /**
     * 生成带参数的二维码
     * @param data 请求参数
     * @returns
     * @description 目前有2种类型的二维码：1、临时二维码，是有过期时间的，最长可以设置为在二维码生成后的30天（即2592000秒）后过期，但能够生成较多数量。临时二维码主要用于帐号绑定等不要求二维码永久保存的业务场景 2、永久二维码，是无过期时间的，但数量较少（目前为最多10万个）。永久二维码主要用于适用于帐号绑定、用户来源统计等场景。
     */
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.CREATE, data, { params: { access_token: this.app.access_token } });
        });
    }
    /**
     * 获取二维码的下载器
     * @param ticket 获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
     * @returns
     * @description 通过下载器可以操作http文件
     */
    getDownloader(ticket) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = new URL(this.SHOW_QRCODE);
            url.searchParams.append('ticket', ticket);
            url.searchParams.append('access_token', this.wx.access_token);
            return new Downloader_1.Downloader(url);
        });
    }
    /**
     * 通过ticket换取二维码的URL
     * @param ticket 获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码
     * @returns
     */
    getUrl(ticket) {
        const url = new URL(this.SHOW_QRCODE);
        url.searchParams.append('ticket', ticket);
        return url.toString();
    }
}
exports.Qrcode = Qrcode;
