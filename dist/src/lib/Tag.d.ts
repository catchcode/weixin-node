import { WX } from '..';
interface responseResult {
    /** 错误码 */
    errcode: number;
    errmsg: string;
}
export declare class Tag {
    private app;
    private readonly CREATE_TAG;
    private readonly GET_TAG;
    private readonly UPDATE_TAG;
    private readonly DELETE_TAG;
    private readonly GET_TAG_USER;
    /** 批量为用户打标签 */
    private readonly BATCH_TAG_GING;
    /** 批量为用户取消标签 */
    private readonly BATCHUN_TAG_GING;
    /** 获取用户身上的标签列表 */
    private readonly GET_USER_TAG;
    private axios;
    constructor(wx: WX);
    /**
     * 创建标签
     * @param name 标签名（30个字符以内）
     * @returns
     */
    createTag(name: string): Promise<{
        tag: {
            /** 标签id，由微信分配  */
            id: number;
            /** 标签名，UTF8编码 */
            name: string;
        };
    }>;
    /**
     * 获取公众号已创建的标签
     * @returns
     */
    getTag(): Promise<{
        tags: {
            /** 标签id，由微信分配 */
            id: number;
            /** 标签名，UTF8编码 */
            name: string;
            /** 此标签下粉丝数 */
            count: number;
        }[];
    }>;
    /**
     * 编辑标签
     * @param update_tag 要编辑的标签
     * @returns
     */
    updateTag(params: {
        tag: {
            /** 标签id，由微信分配  */
            id: number;
            /** 标签名，UTF8编码 */
            name: string;
        };
    }): Promise<responseResult>;
    /**
     * 删除标签
     */
    delTag(id: number): Promise<responseResult>;
    /**
     * 获取标签下粉丝列表
     * @param tagid 标签ID
     * @param next_openid 第一个拉取的OPENID，不填默认从头开始拉取
     */
    getTagUser(params: {
        tagid: number;
        next_openid?: string;
    }): Promise<{
        /** 这次获取的粉丝数量  */
        count: number;
        data: {
            /** 粉丝列表 */
            openid: string[];
        };
        /** 拉取列表最后一个用户的openid */
        next_openid: string;
    }>;
    /**
     * 批量为用户打标签
     * @param openid_list 粉丝列表
     * @param tagid 标签id
     * @returns
     */
    batchTagGing(params: {
        openid_list: string[];
        tagid: number;
    }): Promise<responseResult>;
    /**
     * 批量为用户取消标签
     * @param openid_list 粉丝列表
     * @param tagid 标签id
     * @returns
     */
    batchunTagGing(params: {
        openid_list: string[];
        tagid: number;
    }): Promise<responseResult>;
    /**
     * 获取用户身上的标签列表
     * @param openid 用户 id
     * @returns
     */
    getUserTags(openid: string): Promise<{
        tagid_list: number[];
    }>;
}
export {};
