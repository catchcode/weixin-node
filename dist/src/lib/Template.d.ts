import { WX } from "..";
export declare class Template {
    private readonly API_SET_INDUSTRY;
    private readonly GET_INDUSTRY;
    private readonly API_ADD_TEMPLATE;
    private readonly GET_ALL_PRIVATE_TEMPLATE;
    private readonly DEL_PRIVATE_TEMPLATE;
    private readonly SEND;
    private axios;
    private app;
    constructor(wx: WX);
    /**
     * 设置行业可在微信公众平台后台完成，每月可修改行业1次，帐号仅可使用所属行业中相关的模板，为方便第三方开发者，提供通过接口调用的方式来修改账号所属行业.
     * @see https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html#0
     * @param params
     * @returns
     */
    setIndustry(params: {
        /** 公众号模板消息所属行业编号 */
        industry_id1: string;
        /** 公众号模板消息所属行业编号 */
        industry_id2: string;
    }): Promise<import("axios").AxiosResponse<any>>;
    /**
     * 获取帐号设置的行业信息。可登录微信公众平台，在公众号后台中查看行业信息。
     * 为方便第三方开发者，提供通过接口调用的方式来获取帐号所设置的行业信息
     */
    getIndustry(): Promise<{
        /** 帐号设置的主营行业 */
        "primary_industry": {
            "first_class": string;
            "second_class": string;
        };
        /** 帐号设置的副营行业 */
        "secondary_industry": {
            "first_class": string;
            "second_class": string;
        };
    }>;
    /**
     * 获得模板ID
     * 从行业模板库选择模板到帐号后台，获得模板ID的过程可在微信公众平台后台完成。
     * 为方便第三方开发者，提供通过接口调用的方式来获取模板ID.
     * @param template_id_short 模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式
     * @returns
     */
    getID(template_id_short: string): Promise<{
        "errcode": 0;
        "errmsg": "ok";
        "template_id": string;
    }>;
    /**
     * 获取模板列表
     * 获取已添加至帐号下所有模板列表，可在微信公众平台后台中查看模板列表信息。
     * 为方便第三方开发者，提供通过接口调用的方式来获取帐号下所有模板信息.
     * @returns
     */
    getList(): Promise<{
        "template_list": [
            {
                /** 模板ID */
                "template_id": string;
                /** 模板标题 */
                "title": string;
                /** 模板所属行业的一级行业 */
                "primary_industry": string;
                /** 模板所属行业的二级行业 */
                "deputy_industry": string;
                /** 模板内容 */
                "content": string;
                /** 模板示例 */
                "example": string;
            }
        ];
    }>;
    /**
     * 删除模板
     * 删除模板可在微信公众平台后台完成，为方便第三方开发者，提供通过接口调用的方式来删除某帐号下的模板.
     */
    delById(template_id: string): Promise<{
        "errcode": 0;
        "errmsg": "ok";
    }>;
    /**
     * 发送模板消息
     * @param params 请求参数
     * @returns
     * @description url和miniprogram都是非必填字段，若都不传则模板无跳转；若都传，会优先跳转至小程序。开发者可根据实际需要选择其中一种跳转方式即可。当用户的微信客户端版本不支持跳小程序时，将会跳转至url。
     */
    send(params: {
        /** 接收者openid */
        "touser": string;
        /** 模板ID */
        "template_id": string;
        /** 模板跳转链接（海外帐号没有跳转能力） */
        "url"?: string;
        /** 跳小程序所需数据，不需跳小程序可不用传该数据 */
        "miniprogram"?: {
            /** 所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系，暂不支持小游戏） */
            "appid": string;
            /** 所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar），要求该小程序已发布，暂不支持小游戏 */
            "pagepath": string;
        };
        /** 模板数据 */
        "data": {
            [key: string]: {
                /** 值 */
                "value": string;
                /** 模板内容字体颜色，不填默认为黑色 */
                "color"?: string;
            };
        };
    }): Promise<{
        "errcode": 0;
        "errmsg": "ok";
        "msgid": number;
    }>;
}
