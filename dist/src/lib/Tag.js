"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tag = void 0;
const WXAxios_1 = require("../tool/WXAxios");
class Tag {
    constructor(wx) {
        // 标签管理
        this.CREATE_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/create';
        this.GET_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/get';
        this.UPDATE_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/update';
        this.DELETE_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/delete';
        this.GET_TAG_USER = 'https://api.weixin.qq.com/cgi-bin/user/tag/get';
        // 用户管理
        /** 批量为用户打标签 */
        this.BATCH_TAG_GING = 'https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging';
        /** 批量为用户取消标签 */
        this.BATCHUN_TAG_GING = 'https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging';
        /** 获取用户身上的标签列表 */
        this.GET_USER_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/getidlist';
        this.app = wx;
        this.axios = WXAxios_1.create_axios(wx);
    }
    /**
     * 创建标签
     * @param name 标签名（30个字符以内）
     * @returns
     */
    createTag(name) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.CREATE_TAG, { tag: { name } });
        });
    }
    /**
     * 获取公众号已创建的标签
     * @returns
     */
    getTag() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.get(this.GET_TAG);
        });
    }
    /**
     * 编辑标签
     * @param update_tag 要编辑的标签
     * @returns
     */
    updateTag(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.UPDATE_TAG, params);
        });
    }
    /**
     * 删除标签
     */
    delTag(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = { tag: { id } };
            return this.axios.post(this.DELETE_TAG, data);
        });
    }
    /**
     * 获取标签下粉丝列表
     * @param tagid 标签ID
     * @param next_openid 第一个拉取的OPENID，不填默认从头开始拉取
     */
    getTagUser(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.GET_TAG_USER, params);
        });
    }
    // ================================ 用户管理 ==========================================
    /**
     * 批量为用户打标签
     * @param openid_list 粉丝列表
     * @param tagid 标签id
     * @returns
     */
    batchTagGing(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.BATCH_TAG_GING, params);
        });
    }
    /**
     * 批量为用户取消标签
     * @param openid_list 粉丝列表
     * @param tagid 标签id
     * @returns
     */
    batchunTagGing(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.BATCHUN_TAG_GING, params);
        });
    }
    /**
     * 获取用户身上的标签列表
     * @param openid 用户 id
     * @returns
     */
    getUserTags(openid) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.GET_USER_TAG, { openid });
        });
    }
}
exports.Tag = Tag;
