"use strict";
/*
 * @Descripttion: 永久素材对象
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-02 13:50:31
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-05 12:24:42
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Material = void 0;
const WXAxios_1 = require("../tool/WXAxios");
const curl_1 = require("../tool/curl");
class Material {
    constructor(wx) {
        this.wx = wx;
        this.ADD_NEWS = 'https://api.weixin.qq.com/cgi-bin/material/add_news';
        this.UPLOADIMG = 'https://api.weixin.qq.com/cgi-bin/media/uploadimg';
        this.ADD_MATERIAL = 'https://api.weixin.qq.com/cgi-bin/material/add_material';
        this.GET_MATERIAL = 'https://api.weixin.qq.com/cgi-bin/material/get_material';
        this.DEL_MATERIAL = 'https://api.weixin.qq.com/cgi-bin/material/del_material';
        this.UPDATE_NEWS = 'https://api.weixin.qq.com/cgi-bin/material/update_news';
        this.GET_MATERIALCOUNT = 'https://api.weixin.qq.com/cgi-bin/material/get_materialcount';
        this.BATCHGET_MATERIAL = 'https://api.weixin.qq.com/cgi-bin/material/batchget_material';
        this.axios = WXAxios_1.create_axios(wx);
    }
    /** 新增永久图文素材 */
    addNews(articles) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!(articles instanceof Array)) {
                articles = [articles];
            }
            return this.axios.post(this.ADD_NEWS, { articles });
        });
    }
    /**
     * 上传图文消息内的图片获取URL
     * @description 本接口所上传的图片不占用公众号的素材库中图片数量的100000个的限制。图片仅支持jpg/png格式，大小必须在1MB以下。
     */
    uploadImg(file) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = new URL(this.UPLOADIMG);
            url.searchParams.append('access_token', this.wx.access_token);
            return curl_1.curlUpload(file, url);
        });
    }
    /**
     * 新增其他类型永久素材
     * 上传视频请使用 addMaterialVideo 方法
     */
    addMaterial(file, type = 'image') {
        return __awaiter(this, void 0, void 0, function* () {
            const url = new URL(this.ADD_MATERIAL);
            url.searchParams.append('access_token', this.wx.access_token);
            url.searchParams.append('type', type);
            return curl_1.curlUpload(file, url);
        });
    }
    /**
     *
     * @param file
     * @param description
     * @returns
     */
    addMaterialVideo(file, description) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = new URL(this.ADD_MATERIAL);
            url.searchParams.append('access_token', this.wx.access_token);
            url.searchParams.append('type', 'video');
            return curl_1.curlUploadVideo(file, url, description);
        });
    }
    // =================================================================================
    /**
     * 获取永久素材
     * @param media_id 要获取的素材的media_id
     */
    getMaterial(media_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.GET_MATERIAL, { media_id });
        });
    }
    /**
     * 删除永久素材
     * @param media_id
     * @description 在新增了永久素材后，开发者可以根据本接口来删除不再需要的永久素材，节省空间。
     */
    delMaterial(media_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.DEL_MATERIAL, { media_id });
        });
    }
    /**
     * 修改永久图文素材
     * @param news 参数
     * @returns
     */
    update_news(news) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.UPDATE_NEWS, news);
        });
    }
    /**
     * 获取素材总数
     * @returns
     */
    getMaterialcount() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.get(this.GET_MATERIALCOUNT);
        });
    }
    /**
     * 获取素材列表
     * @param params
     * @returns
     * @see https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/Get_materials_list.html
     */
    batchgetMaterial(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.BATCHGET_MATERIAL, params);
        });
    }
}
exports.Material = Material;
