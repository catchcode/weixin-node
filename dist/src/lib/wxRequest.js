"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Request = void 0;
const parser_1 = require("../tool/parser");
/** 微信推送过来的数据 */
class Request {
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} xml_str 微信推送过来的字符串（xml格式）
     */
    constructor(xml_str) {
        this.data = parser_1.toJSON(xml_str);
    }
    get text() { return this.data; }
    get image() { return this.data; }
    get voice() { return this.data; }
    get shortVideo() { return this.data; }
    get location() { return this.data; }
    get link() { return this.data; }
    /**
     * 判断推送过来的消息是不是某种类型
     * @param type 消息类型
     * @returns
     */
    is(type) {
        if (!(type instanceof Array)) {
            type = [type];
        }
        return type.indexOf(this.data.MsgType) != -1;
    }
    /**
     * 判断推送过来的消息是不是某种事件类型
     * @param type 事件类型
     * @returns
     * @description 内部已经做了对消息类型是 event 的判断，无需重复判断
     */
    isEvent(type) {
        if (!(type instanceof Array)) {
            type = [type];
        }
        return this.is('event') && type.indexOf(this.data.Event) != -1;
    }
}
exports.Request = Request;
