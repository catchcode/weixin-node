/**
 * 负责维护 access_token 的有效性
 */
export default class AccessToken {
    appid: string;
    secret: string;
    interval: number;
    /** 获取成功的访问令牌 */
    access_token: string | null;
    /** 其值是刷新的结果的 Promise ，可以使用 await 等待它 */
    private refresh_result;
    /** 请求的地址 */
    private readonly TOKEN;
    private axios;
    /** 构造函数 */
    constructor(appid: string, secret: string, interval?: number);
    /**
     * 刷新令牌，成功返回 true  失败返回 false
     */
    refresh(): Promise<boolean>;
    /**
     * 自动刷新access_token
     */
    private auto;
    /**
     * 等待刷新完毕才返回。
     */
    wait(): Promise<boolean>;
    /** 重写 string 方法 */
    toString(): string;
}
