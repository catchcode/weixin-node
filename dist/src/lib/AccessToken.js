"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const WXAxios_1 = require("../tool/WXAxios");
const echo_1 = require("../tool/echo");
/**
 * 负责维护 access_token 的有效性
 */
class AccessToken {
    /** 构造函数 */
    constructor(appid, secret, interval = 7000) {
        this.appid = appid;
        this.secret = secret;
        this.interval = interval;
        /** 获取成功的访问令牌 */
        this.access_token = null;
        /** 其值是刷新的结果的 Promise ，可以使用 await 等待它 */
        this.refresh_result = Promise.resolve(true);
        /** 请求的地址 */
        this.TOKEN = 'https://api.weixin.qq.com/cgi-bin/token';
        this.axios = WXAxios_1.create_axios(); // 处理响应结果
        this.auto();
        this.refresh_result = this.refresh();
    }
    /**
     * 刷新令牌，成功返回 true  失败返回 false
     */
    refresh() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // 通过 ajax 请求接口，获取结果，如果 errcode 不为 0 ，则表示 接口请求失败
                const data = yield this.axios.get(this.TOKEN, {
                    params: {
                        appid: this.appid,
                        secret: this.secret,
                        grant_type: "client_credential"
                    }
                });
                // 如果响应拦截器没有抛出异常，则以正常获取到访问令牌
                this.access_token = data.access_token;
                echo_1.echo(`刷新访问令牌,有效期[${data.expires_in}]秒:${this.access_token}`);
                return true;
            }
            catch (e) {
                echo_1.echo(`刷新访问令牌失败:${JSON.stringify(e)}`);
                return false;
            }
        });
    }
    /**
     * 自动刷新access_token
     */
    auto() {
        /**
         * 定时器第一次执行时不会立刻执行。
         */
        setInterval(() => {
            this.refresh_result = this.refresh(); // 由于 refresh 是异步函数，将它的 permise 保存起来，即可得知异步函数的结果
        }, this.interval * 1000); // interval 的单位是 秒
        return this;
    }
    /**
     * 等待刷新完毕才返回。
     */
    wait() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.refresh_result;
        });
    }
    /** 重写 string 方法 */
    toString() {
        return this.access_token || '';
    }
}
exports.default = AccessToken;
