"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mass = void 0;
const WXAxios_1 = require("../tool/WXAxios");
/** 群发消息接口 */
class Mass {
    constructor(app) {
        this.SEND = 'https://api.weixin.qq.com/cgi-bin/message/mass/send';
        this.SENDAll = 'https://api.weixin.qq.com/cgi-bin/message/mass/sendall';
        this.UPLOADVIDEO = 'https://api.weixin.qq.com/cgi-bin/media/uploadvideo';
        this.DELETE = 'https://api.weixin.qq.com/cgi-bin/message/mass/delete';
        this.PREVIEW = 'https://api.weixin.qq.com/cgi-bin/message/mass/preview';
        this.GETSTATUS = 'https://api.weixin.qq.com/cgi-bin/message/mass/get';
        this.GETSPEED = 'https://api.weixin.qq.com/cgi-bin/message/mass/speed/get';
        this.SETSPEED = 'https://api.weixin.qq.com/cgi-bin/message/mass/speed/set';
        this.app = app;
        /** 创建一个自己的axios实例 */
        this.axios = WXAxios_1.create_axios(app);
    }
    send(params) {
        return this.axios.post(this.SEND, params);
    }
    sendAll(params) {
        return this.axios.post(this.SENDAll, params);
    }
    /** 通过 openId 发送图文 */
    sendNewsByOpenId(params) {
        return this.send(Object.assign(Object.assign({}, params), { msgtype: 'mpnews' }));
    }
    /** 通过 openId 群发文本 */
    sendTextByOpenId(params) {
        return this.send(Object.assign(Object.assign({}, params), { msgtype: 'text' }));
    }
    /** 通过 openId 群发语音/音频 */
    sendVoice(params) {
        return this.send(Object.assign(Object.assign({}, params), { msgtype: 'voice' }));
    }
    /** 通过 opendId 群发图片 */
    sendImageByOpenId(params) {
        return this.send(Object.assign(Object.assign({}, params), { msgtype: 'image' }));
    }
    /**
     * 通过 openId 群发视频 注意此处media_id需通过uploadvideo方法得到
     *  */
    sendVideoByOpenId(params) {
        return this.send(Object.assign(Object.assign({}, params), { msgtype: 'mpvideo' }));
    }
    /** 通过 openid 群发卡券 */
    sendWxcardByOpenId(params) {
        return this.send(Object.assign(Object.assign({}, params), { msgtype: 'wxcard' }));
    }
    // ========================= 通过 标签进行群发 ===========================
    /** 通过 tag 发送图文消息 */
    sendNewsByTag(params) {
        return this.sendAll(Object.assign(Object.assign({}, params), { msgtype: "mpnews" }));
    }
    /** 通过 tag 发送文本消息 */
    sendTextByTag(params) {
        return this.sendAll(Object.assign(Object.assign({}, params), { msgtype: "text" }));
    }
    /** 通过 tag 发送语音/音频消息 （注意此处media_id需通过素材管理->新增素材来得到） */
    sendVoiceByTag(params) {
        return this.sendAll(Object.assign(Object.assign({}, params), { msgtype: "voice" }));
    }
    /** 通过 tag 发送图片（注意此处media_id需通过素材管理->新增素材来得到） */
    sendImageByTag(params) {
        return this.sendAll(Object.assign(Object.assign({}, params), { msgtype: "image" }));
    }
    /**
     * 通过 tag 发送视频（注意此处media_id需通过uploadvideo方法得到）
     */
    sendVideoByTag(params) {
        return this.sendAll(Object.assign(Object.assign({}, params), { msgtype: "mpvideo" }));
    }
    /** 卡券消息（注意图文消息的media_id需要通过上述方法来得到） */
    sendWXcardByTag(params) {
        return this.sendAll(Object.assign(Object.assign({}, params), { msgtype: "wxcard" }));
    }
    // ============   特别接口  ===================
    /** 将素材管理的 media_id 转换成 群发视频的 media_id */
    uploadvideo(params) {
        return this.axios.post(this.UPLOADVIDEO, params);
    }
    /** 删除群发【订阅号与服务号认证后均可用】 */
    delete(params) {
        return this.axios.post(this.DELETE, params);
    }
    /** 预览接口【订阅号与服务号认证后均可用】 */
    preview(params) {
        return this.axios.post(this.PREVIEW, params);
    }
    /** 查询群发消息发送状态【订阅号与服务号认证后均可用】 */
    getStatus(params) {
        return this.axios.post(this.GETSTATUS, params);
    }
    /** 获取群发速度 */
    getSpeed() {
        return this.axios.get(this.GETSPEED);
    }
    /** 设置群发速度 0=80w/分钟 1=60w/分钟 2=45w/分钟 3=30w/分钟 4=10w/分钟 */
    setSpeed(params) {
        return this.axios.post(this.SETSPEED, params);
    }
}
exports.Mass = Mass;
