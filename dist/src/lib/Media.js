"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * @Descripttion: 素材管理
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-13 17:53:56
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-02 13:34:43
 */
const fs_1 = __importDefault(require("fs"));
const os_1 = __importDefault(require("os"));
const path_1 = __importDefault(require("path"));
const form_data_1 = __importDefault(require("form-data"));
const WXAxios_1 = require("../tool/WXAxios");
const Downloader_1 = require("../tool/Downloader");
/** 临时素材管理类 */
class Media {
    constructor(wx) {
        this.wx = wx;
        this.GET = 'https://api.weixin.qq.com/cgi-bin/media/get';
        this.UPLOAD = 'https://api.weixin.qq.com/cgi-bin/media/upload';
        this.axios = WXAxios_1.create_axios(wx);
    }
    /**
     * 通过URL上传文件【不稳定】
     * @param type
     * @param url 需要上传的文件
     * @param suffix 保存的临时文件名后缀，上传素材文件的时候，如果不指定这个参数可能上传失败，报错 40005
     * @description 内部通过下
     */
    uploadByURL(type, url, suffix) {
        return __awaiter(this, void 0, void 0, function* () {
            const dir = yield fs_1.default.promises.mkdtemp(path_1.default.join(os_1.default.tmpdir(), 'weixin-nodejs-'));
            const r = Math.random();
            const t = Date.now();
            const _path = path_1.default.join(dir, `${t}-${r}${suffix}`);
            console.log(_path);
            const downloader = new Downloader_1.Downloader(url);
            yield downloader.save(_path);
            return this.upload(type, fs_1.default.createReadStream(_path));
        });
    }
    /**
     *
     * @param media 只读文件流
     * @param type 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     * @returns 返回微信服务器响应结果
     * @description 这个方法适用与上传本地文件
     */
    upload(type, media) {
        return __awaiter(this, void 0, void 0, function* () {
            const form = new form_data_1.default();
            form.append('media', media);
            return new Promise((resolve, reject) => {
                form.getLength((err, length) => __awaiter(this, void 0, void 0, function* () {
                    this.axios.post(this.UPLOAD, form, {
                        params: { type },
                        headers: Object.assign(Object.assign({}, form.getHeaders()), { 'Content-Length': length })
                    }).then((data) => {
                        resolve(data);
                    }).catch(e => {
                        reject(e);
                    });
                }));
            });
        });
    }
    /**
     * 上传图片(upload函数的简写)
     * @param media 要上传的只读文件流
     * @returns
     */
    uploadImage(media) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.upload('image', media);
        });
    }
    /**
     * 获取一个media_id指向的文件下载器，后续可以通过这个下载器操作这个文件。
     * @param media_id 媒体文件ID
     * @returns 下载器
     * @description 获取临时素材的下载器,此方法适用于图片、语音、缩略图，不适用于 视频
     */
    getDownloader(media_id) {
        const url = new URL(this.GET);
        url.searchParams.append('media_id', media_id);
        url.searchParams.append('access_token', this.wx.access_token);
        return new Downloader_1.Downloader(url);
    }
    /**
     * 获取视频文件的下载器，后续可以通过这个下载器操作这个视频文件。
     * @param media_id 视频媒体文件ID
     * @returns
     */
    getDownloaderOfVideo(media_id) {
        return __awaiter(this, void 0, void 0, function* () {
            const { video_url } = yield this.getVideo(media_id);
            const url = new URL(video_url);
            url.searchParams.append('access_token', this.wx.access_token);
            return new Downloader_1.Downloader(url);
        });
    }
    /**
     * 请求下载视频媒体文件接口
     * @param media_id 视频媒体文件ID
     * @returns
     */
    getVideo(media_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.get(this.GET, { params: { media_id }, responseType: 'json' });
        });
    }
}
exports.default = Media;
