import { WX } from "..";
import { Downloader } from "../tool/Downloader";
declare type action_name = 'QR_SCENE' | 'QR_STR_SCENE' | 'QR_LIMIT_SCENE' | 'QR_LIMIT_STR_SCENE';
export declare class Qrcode {
    private wx;
    private app;
    private readonly CREATE;
    private readonly SHOW_QRCODE;
    private axios;
    constructor(wx: WX);
    /**
     * 生成带参数的二维码
     * @param data 请求参数
     * @returns
     * @description 目前有2种类型的二维码：1、临时二维码，是有过期时间的，最长可以设置为在二维码生成后的30天（即2592000秒）后过期，但能够生成较多数量。临时二维码主要用于帐号绑定等不要求二维码永久保存的业务场景 2、永久二维码，是无过期时间的，但数量较少（目前为最多10万个）。永久二维码主要用于适用于帐号绑定、用户来源统计等场景。
     */
    create(data: {
        /** 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天），此字段如果不填，则默认有效期为30秒。 */
        expire_seconds?: 604800;
        /** 二维码类型，QR_SCENE为临时的整型参数值，QR_STR_SCENE为临时的字符串参数值，QR_LIMIT_SCENE为永久的整型参数值，QR_LIMIT_STR_SCENE为永久的字符串参数值 */
        action_name: action_name;
        /** 二维码详细信息 */
        action_info: {
            scene: {
                /** 场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000） */
                scene_id: number;
            } | {
                /** 场景值ID（字符串形式的ID），字符串类型，长度限制为1到64 */
                scene_str: string;
            };
        };
    }): Promise<{
        /** 获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。 */
        ticket: string;
        /** 该二维码有效时间，以秒为单位。 最大不超过2592000（即30天）。 */
        expire_seconds: number;
        /** 二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片 */
        url: string;
    }>;
    /**
     * 获取二维码的下载器
     * @param ticket 获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
     * @returns
     * @description 通过下载器可以操作http文件
     */
    getDownloader(ticket: string): Promise<Downloader>;
    /**
     * 通过ticket换取二维码的URL
     * @param ticket 获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码
     * @returns
     */
    getUrl(ticket: string): string;
}
export {};
