/// <reference types="node" />
import fs from 'fs';
import { WX } from '..';
import { Downloader } from '../tool/Downloader';
export declare type types = 'image' | 'voice' | 'video' | 'thumb';
/** 临时素材管理类 */
export default class Media {
    wx: WX;
    private GET;
    private UPLOAD;
    private axios;
    constructor(wx: WX);
    /**
     * 通过URL上传文件【不稳定】
     * @param type
     * @param url 需要上传的文件
     * @param suffix 保存的临时文件名后缀，上传素材文件的时候，如果不指定这个参数可能上传失败，报错 40005
     * @description 内部通过下
     */
    uploadByURL(type: types, url: string | URL, suffix?: '.jpg' | '.txt' | '.png' | '.jpeg' | string): Promise<{
        /** 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图） */
        type: types;
        /** 媒体文件上传后，获取标识 */
        media_id: string;
        /** 缩略图文件标识 */
        thumb_media_id: string;
        /** 媒体文件上传时间戳 */
        created_at: string;
    }>;
    /**
     *
     * @param media 只读文件流
     * @param type 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     * @returns 返回微信服务器响应结果
     * @description 这个方法适用与上传本地文件
     */
    upload(type: types, media: fs.ReadStream): Promise<{
        /** 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图） */
        type: types;
        /** 媒体文件上传后，获取标识 */
        media_id: string;
        /** 缩略图文件标识 */
        thumb_media_id: string;
        /** 媒体文件上传时间戳 */
        created_at: string;
    }>;
    /**
     * 上传图片(upload函数的简写)
     * @param media 要上传的只读文件流
     * @returns
     */
    uploadImage(media: fs.ReadStream): Promise<{
        /** 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图） */
        type: types;
        /** 媒体文件上传后，获取标识 */
        media_id: string;
        /** 缩略图文件标识 */
        thumb_media_id: string;
        /** 媒体文件上传时间戳 */
        created_at: string;
    }>;
    /**
     * 获取一个media_id指向的文件下载器，后续可以通过这个下载器操作这个文件。
     * @param media_id 媒体文件ID
     * @returns 下载器
     * @description 获取临时素材的下载器,此方法适用于图片、语音、缩略图，不适用于 视频
     */
    getDownloader(media_id: string): Downloader;
    /**
     * 获取视频文件的下载器，后续可以通过这个下载器操作这个视频文件。
     * @param media_id 视频媒体文件ID
     * @returns
     */
    getDownloaderOfVideo(media_id: string): Promise<Downloader>;
    /**
     * 请求下载视频媒体文件接口
     * @param media_id 视频媒体文件ID
     * @returns
     */
    getVideo(media_id: string): Promise<{
        /** 如果返回的是视频消息素材，则内容如下 */
        video_url: string;
    }>;
}
