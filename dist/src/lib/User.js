"use strict";
/*
 * @Descripttion: 用户管理
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-17 10:21:18
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-02 21:36:23
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const WXAxios_1 = require("../tool/WXAxios");
class User {
    constructor(wx) {
        /** 获取用户基本信息 */
        this.INFO = 'https://api.weixin.qq.com/cgi-bin/user/info';
        /** 批量获取用户基本信息 */
        this.BATCH_GET = 'https://api.weixin.qq.com/cgi-bin/user/info/batchget';
        /** 设置用户备注名 */
        this.UPDATE_REMAKE = 'https://api.weixin.qq.com/cgi-bin/user/info/updateremark';
        /** 批量获取 */
        this.GET_BATCH_USER = 'https://api.weixin.qq.com/cgi-bin/user/get';
        this.app = wx;
        this.axios = WXAxios_1.create_axios(wx);
    }
    /**
     * 获取用户基本信息(UnionID机制)
     * @param openid 普通用户的标识，对当前公众号唯一
     * @param lang 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     * @returns
     */
    getInfo(openid, lang = 'zh_CN') {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.get(this.INFO, { params: { openid, lang, } });
        });
    }
    /**
     * 批量获取用户基本信息
     * @param openid_array 用户的标识组成的数组，对当前公众号唯一
     * @param lang 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     * @description 开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条。
     */
    getBatchInfo(openid_array, lang = 'zh_CN') {
        return __awaiter(this, void 0, void 0, function* () {
            const user_list = openid_array.map(openid => {
                return {
                    openid,
                    lang
                };
            });
            const data = { user_list };
            return this.axios.post(this.BATCH_GET, data);
        });
    }
    /**
     * 获取用户列表
     * @param next_openid 第一个拉取的OPENID，不填默认从头开始拉取
     * @returns 正确时返回JSON数据包
     * @description 公众号可通过本接口来获取帐号的关注者列表，关注者列表由一串OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
     */
    getBatchUser(next_openid) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.get(this.GET_BATCH_USER, { params: { next_openid } });
        });
    }
    /**
     * 设置用户备注名
     * @param openid 用户标识
     * @param remark 新的备注名，长度必须小于30字符
     * @returns
     */
    setMark(openid, remark) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = { openid, remark };
            return this.axios.post(this.UPDATE_REMAKE, data);
        });
    }
}
exports.User = User;
