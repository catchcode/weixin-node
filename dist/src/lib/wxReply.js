"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Reply = exports.MsgTypes = void 0;
/*
 * @Descripttion: 描述
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-13 12:55:29
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-19 13:13:36
 */
var MsgTypes;
(function (MsgTypes) {
    /** 回复文本消息 */
    MsgTypes["text"] = "text";
    /** 回复图片消息 */
    MsgTypes["image"] = "image";
    /** 回复语音消息 */
    MsgTypes["voice"] = "voice";
    /** 回复视频消息 */
    MsgTypes["video"] = "video";
    /** 回复音乐消息 */
    MsgTypes["music"] = "music";
    /** 回复图文消息 */
    MsgTypes["news"] = "news";
})(MsgTypes = exports.MsgTypes || (exports.MsgTypes = {}));
class Reply {
    constructor(
    /* 开发者微信号 */
    FromUserName, 
    /* 接收方帐号（收到的OpenID） */
    ToUserName, 
    /** 消息创建时间 （整型） */
    CreateTime = Date.now().toString()) {
        this.FromUserName = FromUserName;
        this.ToUserName = ToUserName;
        this.CreateTime = CreateTime;
        this.data = {};
        /** 回复的数据（JSON） */
        this.data = { FromUserName, ToUserName, CreateTime };
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} Content 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
     * @return {string} 返回文本格式的xml字符串
     * @description 回复文本消息
     */
    text(Content) {
        return Object.assign(Object.assign({}, this.data), { Content, MsgType: MsgTypes.text });
    }
    /**
     * @description 回复图片消息
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} MediaId 通过素材管理中的接口上传多媒体文件，得到的id。
     * @return {string} 返回图片格式的xml字符串
     */
    image(MediaId) {
        return Object.assign(Object.assign({}, this.data), { Image: { MediaId }, MsgType: MsgTypes.image });
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} MediaId
     * @return {string} 返回语音格式的xml字符串
     * @description 回复语音消息
     */
    voice(MediaId) {
        return Object.assign(Object.assign({}, this.data), { Voice: { MediaId }, MsgType: MsgTypes.voice });
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {Video} video
     * @return {string} 返回视频格式的xml字符串
     * @description 回复视频消息
     */
    video(Video) {
        return Object.assign(Object.assign({}, this.data), { Video, MsgType: MsgTypes.video });
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {MusicItem} option
     * @return {string} 返回音乐格式的xml字符串
     * @description 回复音乐
     */
    music(Music) {
        return Object.assign(Object.assign({}, this.data), { Music, MsgType: MsgTypes.music });
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {Articles} item
     * @return {string} 返回图文乐式的xml字符串
     * @description 回复图文
     */
    news(item) {
        if (!(item instanceof Array)) {
            item = [item];
        }
        return Object.assign(Object.assign({}, this.data), { Articles: {
                item
            }, ArticleCount: String(item.length), MsgType: MsgTypes.news });
    }
    /**
     * 进行拦截，不在继续执行后续插件【未写】
     */
    intercept() {
    }
}
exports.Reply = Reply;
