"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Plugin = void 0;
class Plugin {
    /** 构造函数 */
    constructor(
    /** WX 实例 */
    app, 
    /** 微信服务器的请求对象，可以获取到请求参数 */
    request, 
    /** 一个响应微信服务器的工具对象 */
    reply, 
    /** 基于发送方openId创建的一个数据仓库 */
    store) {
        this.app = app;
        this.request = request;
        this.reply = reply;
        this.store = store;
    }
    /** request 的缩写 */
    get req() {
        return this.request;
    }
    /** request.data 的缩写 */
    get params() {
        return this.request.data;
    }
    /** 当插件被 wx 实例使用的时候被调用，它只会被调用一次，也就是调用 wx.use 时触发这个事件 */
    static onUse(app) {
    }
    /**
     * 每次收到微信服务器的消息时，都会调用这个函数，用来判断插件是否要处理这条信息
     * @returns {boolean} 返回真，将执行 process 方法,返回假，将执行下一个插件
     */
    test() {
        return true;
    }
    ;
    /**
     * 插件的处理逻辑，
     * @return {object|string|void} 返回object 表示响应成功，其内容将转换成 xml 响应给微信服务器，回复 string 类型则将其封装成 text 类型的响应结构，响应给微信服务器，返回其他类型和void类型一样，表示不处理
     */
    process() { }
    ;
}
exports.Plugin = Plugin;
/** 是否禁用 */
Plugin.disable = false;
/** 插件的描述 */
Plugin.description = "插件没有编写描述...";
/**
 * 插件的优先级
 * 优先级越大 则优先处理消息，默认值是 1000
 */
Plugin.priority = 1000;
