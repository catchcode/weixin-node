import { Basic as PushBasic } from './wxRequest';
interface Basic extends PushBasic {
    /** 事件类型 */
    readonly Event: EventTypes;
    /** 事件KEY值 */
    readonly EventKey: string;
}
export declare type EventTypes = 'CLICK' | 'VIEW' | 'SCAN' | 'scancode_push' | 'scancode_waitmsg' | 'pic_sysphoto' | 'pic_photo_or_album' | 'pic_weixin' | 'location_select' | 'view_miniprogram' | 'subscribe';
/** 点击菜单拉取消息时的事件推送 */
export interface Click extends Basic {
}
/** 点击菜单拉取消息时的事件推送 */
export interface VIEW extends Basic {
    /** 指菜单ID，如果是个性化菜单，则可以通过这个字段，知道是哪个规则的菜单被点击了。 */
    readonly MenuID: string;
}
/** scancode_push：扫码推事件的事件推送 */
export interface ScancodePush extends Basic {
    /** 扫描信息 */
    readonly ScanCodeInfo: string;
    /** 扫描类型，一般是qrcode */
    readonly ScanType: string;
    /** 扫描结果，即二维码对应的字符串信息 */
    readonly ScanResult: string;
}
/** scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框的事件推送 */
export interface ScancodeWaitMsg extends Basic {
    /** 扫描信息 */
    readonly ScanCodeInfo: string;
    /** 扫描类型，一般是qrcode */
    readonly ScanType: string;
    /** 扫描结果，即二维码对应的字符串信息 */
    readonly ScanResult: string;
}
/** pic_sysphoto：弹出系统拍照发图的事件推送 */
export interface PicSysPhoto extends Basic {
    /** 发送的图片信息 */
    readonly SendPicsInfo: string;
    /** 发送的图片数量发送的图片数量 */
    readonly Count: string;
    /** 图片列表 */
    readonly PicList: string;
    /** 图片的MD5值，开发者若需要，可用于验证接收到图片 */
    readonly PicMd5Sum: string;
}
/** pic_photo_or_album：弹出拍照或者相册发图的事件推送 */
export interface PicPhotoOrAlbum extends Basic {
    /** 发送的图片信息 */
    readonly SendPicsInfo: string;
    /** 发送的图片数量发送的图片数量 */
    readonly Count: string;
    /** 图片列表 */
    readonly PicList: string;
    /** 图片的MD5值，开发者若需要，可用于验证接收到图片 */
    readonly PicMd5Sum: string;
}
/** pic_weixin：弹出微信相册发图器的事件推送 */
export interface PicWeixin extends Basic {
    /** 发送的图片信息 */
    readonly SendPicsInfo: string;
    /** 发送的图片数量发送的图片数量 */
    readonly Count: string;
    /** 图片列表 */
    readonly PicList: string;
    /** 图片的MD5值，开发者若需要，可用于验证接收到图片 */
    readonly PicMd5Sum: string;
}
/** location_select：弹出地理位置选择器的事件推送 */
export interface LocationSelect extends Basic {
    /** 发送的位置信息 */
    readonly SendLocationInfo: string;
    /** X坐标信息 */
    readonly Location_X: string;
    /** Y坐标信息 */
    readonly Location_Y: string;
    /** 精度，可理解为精度或者比例尺、越精细的话 scale越高 */
    readonly Scale: string;
    /** 地理位置的字符串信息 */
    readonly Label: string;
    /** 朋友圈POI的名字，可能为空 */
    readonly Poiname: string;
}
/** 点击菜单跳转小程序的事件推送 */
export interface ViewMiniProgram extends Basic {
    /** 菜单ID，如果是个性化菜单，则可以通过这个字段，知道是哪个规则的菜单被点击了 */
    readonly MenuID: string;
}
export declare type All = Click & VIEW & ScancodePush & ScancodeWaitMsg & PicSysPhoto & PicPhotoOrAlbum & PicWeixin & LocationSelect & ViewMiniProgram;
export declare type Any = Click | VIEW | ScancodePush | ScancodeWaitMsg | PicSysPhoto | PicPhotoOrAlbum | PicWeixin | LocationSelect | ViewMiniProgram;
export {};
