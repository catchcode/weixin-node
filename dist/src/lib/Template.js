"use strict";
/*
 * @Descripttion: 微信模板消息
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-06-11 09:47:52
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-06-11 10:57:04
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Template = void 0;
const WXAxios_1 = require("../tool/WXAxios");
class Template {
    constructor(wx) {
        this.API_SET_INDUSTRY = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry";
        this.GET_INDUSTRY = "https://api.weixin.qq.com/cgi-bin/template/get_industry";
        this.API_ADD_TEMPLATE = "https://api.weixin.qq.com/cgi-bin/template/api_add_template";
        this.GET_ALL_PRIVATE_TEMPLATE = "https://api.weixin.qq.com/cgi-bin/template/api_add_template";
        this.DEL_PRIVATE_TEMPLATE = "https://api.weixin.qq.com/cgi-bin/template/del_private_template";
        this.SEND = "https://api.weixin.qq.com/cgi-bin/message/template/send";
        this.app = wx;
        this.axios = WXAxios_1.create_axios(wx);
    }
    /**
     * 设置行业可在微信公众平台后台完成，每月可修改行业1次，帐号仅可使用所属行业中相关的模板，为方便第三方开发者，提供通过接口调用的方式来修改账号所属行业.
     * @see https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html#0
     * @param params
     * @returns
     */
    setIndustry(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.API_SET_INDUSTRY, params);
        });
    }
    /**
     * 获取帐号设置的行业信息。可登录微信公众平台，在公众号后台中查看行业信息。
     * 为方便第三方开发者，提供通过接口调用的方式来获取帐号所设置的行业信息
     */
    getIndustry() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.get(this.GET_INDUSTRY);
        });
    }
    /**
     * 获得模板ID
     * 从行业模板库选择模板到帐号后台，获得模板ID的过程可在微信公众平台后台完成。
     * 为方便第三方开发者，提供通过接口调用的方式来获取模板ID.
     * @param template_id_short 模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式
     * @returns
     */
    getID(template_id_short) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.API_ADD_TEMPLATE, { template_id_short });
        });
    }
    /**
     * 获取模板列表
     * 获取已添加至帐号下所有模板列表，可在微信公众平台后台中查看模板列表信息。
     * 为方便第三方开发者，提供通过接口调用的方式来获取帐号下所有模板信息.
     * @returns
     */
    getList() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.GET_ALL_PRIVATE_TEMPLATE);
        });
    }
    /**
     * 删除模板
     * 删除模板可在微信公众平台后台完成，为方便第三方开发者，提供通过接口调用的方式来删除某帐号下的模板.
     */
    delById(template_id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.DEL_PRIVATE_TEMPLATE, { template_id });
        });
    }
    /**
     * 发送模板消息
     * @param params 请求参数
     * @returns
     * @description url和miniprogram都是非必填字段，若都不传则模板无跳转；若都传，会优先跳转至小程序。开发者可根据实际需要选择其中一种跳转方式即可。当用户的微信客户端版本不支持跳小程序时，将会跳转至url。
     */
    send(params) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.post(this.SEND, params);
        });
    }
}
exports.Template = Template;
