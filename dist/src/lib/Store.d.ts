/** 仓库里的数据类型 */
interface interfaceStoreData {
    fresh?: boolean;
    [key: string]: Object | any;
}
/**
 * 在使用之前需要指定 _path 为
 * 数据仓库，使用openID可以构造一个基于该openID的仓库
 * 每个openID只对应一个仓库
 */
export declare class Store {
    key: string;
    /** 保存仓库的绝对位置 */
    private static _path;
    static get path(): string;
    static set path(_path: string);
    /** 判断目录是否存在，不存在则创建 */
    static check(): void;
    /** 保存单次数据的属性 */
    data: interfaceStoreData;
    /** 构造函数，构造一个唯一的sotre */
    constructor(key: string);
    /** 设置一个键值对 */
    set(key: string, value: any): this;
    /**
     * 合并一个数据，用来代替 set 多次设置
     * @param data 内部采用 ES6 解构语法，所以应该考虑对象浅拷贝的问题
     * @returns
     */
    join(data: interfaceStoreData): this;
    /**
     * 获取这个键的值
     * @param {string} key 键名
     * @param {string} default_value 不存在这个键的时候返回
     */
    get(key: string, default_value?: any): any;
    /** 一次获取所有数据 */
    getAll(): interfaceStoreData;
    /**
     * 清除当前所有数据,
     * @returns
     */
    clean(): this;
    /**
     * 将数据持久化
     * 如果不需要，则无需调用此方法
     */
    save(): void;
    /**
     * 数据持久化保存数据,如果你需要自定义，则覆盖这个【属性】
     * @param unique_key 唯一键，用来区分不同的仓库
     * @param data 保存的数据
     */
    static onWrite: (unique_key: string, data: interfaceStoreData) => void;
    /**
     * 数据持久化读取数据,如果你需要自定义，则覆盖这个【属性】
     * @param {string} unique_key 唯一键，用来区分不同的仓库
     * @returns 返回可以初始化仓库的对象，应该时只包含一层结构的普通数据对象
     */
    static onRead: (unique_key: string) => interfaceStoreData;
}
export {};
