import { Request } from './wxRequest';
import { Any as ReplyAny, Reply } from './wxReply';
import { WX } from '../index';
import { Store } from './Store';
export declare class Plugin {
    /** WX 实例 */
    app: WX;
    /** 微信服务器的请求对象，可以获取到请求参数 */
    request: Request;
    /** 一个响应微信服务器的工具对象 */
    reply: Reply;
    /** 基于发送方openId创建的一个数据仓库 */
    store: Store;
    /** 是否禁用 */
    static disable: boolean;
    /** 插件的描述 */
    static description: string;
    /**
     * 插件的优先级
     * 优先级越大 则优先处理消息，默认值是 1000
     */
    static priority: number;
    /** request 的缩写 */
    get req(): Request;
    /** request.data 的缩写 */
    get params(): import("./wxRequest").All;
    /** 构造函数 */
    constructor(
    /** WX 实例 */
    app: WX, 
    /** 微信服务器的请求对象，可以获取到请求参数 */
    request: Request, 
    /** 一个响应微信服务器的工具对象 */
    reply: Reply, 
    /** 基于发送方openId创建的一个数据仓库 */
    store: Store);
    /** 当插件被 wx 实例使用的时候被调用，它只会被调用一次，也就是调用 wx.use 时触发这个事件 */
    static onUse(app: WX): void;
    /**
     * 每次收到微信服务器的消息时，都会调用这个函数，用来判断插件是否要处理这条信息
     * @returns {boolean} 返回真，将执行 process 方法,返回假，将执行下一个插件
     */
    test(): boolean;
    /**
     * 插件的处理逻辑，
     * @return {object|string|void} 返回object 表示响应成功，其内容将转换成 xml 响应给微信服务器，回复 string 类型则将其封装成 text 类型的响应结构，响应给微信服务器，返回其他类型和void类型一样，表示不处理
     */
    process(): (ReplyAny | string | void) | (Promise<ReplyAny | string | void>);
}
export interface PublicClass {
    new (wx: WX, request: Request, reply: Reply, store: Store): Plugin;
    disable: boolean;
    description: string;
    priority: number;
    onUse: Function;
}
