"use strict";
/*
 * @Descripttion: 类似 session 的存储
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-18 13:08:46
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 13:25:09
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Store = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const echo_1 = require("../tool/echo");
const option_1 = require("../tool/option");
/**
 * 在使用之前需要指定 _path 为
 * 数据仓库，使用openID可以构造一个基于该openID的仓库
 * 每个openID只对应一个仓库
 */
class Store {
    /** 构造函数，构造一个唯一的sotre */
    constructor(key) {
        this.key = key;
        this.data = Store.onRead(key);
    }
    static get path() { return this._path; }
    static set path(_path) {
        if (_path) {
            this._path = _path;
        }
        Store.check();
    }
    /** 判断目录是否存在，不存在则创建 */
    static check() {
        if (!fs_1.default.existsSync(this._path)) {
            fs_1.default.mkdirSync(this._path);
            echo_1.echo(`仓库目录不存在，已创建[${this._path}]`);
        }
    }
    /** 设置一个键值对 */
    set(key, value) {
        this.data[key] = value;
        return this;
    }
    /**
     * 合并一个数据，用来代替 set 多次设置
     * @param data 内部采用 ES6 解构语法，所以应该考虑对象浅拷贝的问题
     * @returns
     */
    join(data) {
        this.data = Object.assign(Object.assign({}, this.data), data);
        return this;
    }
    /**
     * 获取这个键的值
     * @param {string} key 键名
     * @param {string} default_value 不存在这个键的时候返回
     */
    get(key, default_value = null) {
        return this.data[key] || default_value;
    }
    /** 一次获取所有数据 */
    getAll() {
        return this.data;
    }
    /**
     * 清除当前所有数据,
     * @returns
     */
    clean() {
        this.data = {};
        return this;
    }
    /**
     * 将数据持久化
     * 如果不需要，则无需调用此方法
     */
    save() {
        Store.onWrite(this.key, this.data);
    }
}
exports.Store = Store;
/** 保存仓库的绝对位置 */
Store._path = option_1.defaultOption.store_path;
/**
 * 数据持久化保存数据,如果你需要自定义，则覆盖这个【属性】
 * @param unique_key 唯一键，用来区分不同的仓库
 * @param data 保存的数据
 */
Store.onWrite = function (unique_key, data) {
    Store.check();
    try {
        fs_1.default.writeFileSync(path_1.default.join(Store.path, unique_key + '.json'), JSON.stringify(data));
    }
    catch (e) {
        echo_1.echo(`将数据写入至文件中失败，检查路径是否正确或是否具备读写权限。错误信息：${e.message}`);
    }
};
/**
 * 数据持久化读取数据,如果你需要自定义，则覆盖这个【属性】
 * @param {string} unique_key 唯一键，用来区分不同的仓库
 * @returns 返回可以初始化仓库的对象，应该时只包含一层结构的普通数据对象
 */
Store.onRead = function (unique_key) {
    Store.check();
    try {
        const tmp_path = path_1.default.join(Store.path, unique_key + '.json');
        const attach = { fresh: undefined };
        // 判断文件是否存在
        if (!fs_1.default.existsSync(tmp_path)) {
            fs_1.default.writeFileSync(tmp_path, '{}'); // 写入空对象
            attach.fresh = true; // 认为这是一个新鲜的仓库
        }
        else {
            attach.fresh = false; // 认为这不是一个新鲜的仓库
        }
        const data = JSON.parse(fs_1.default.readFileSync(tmp_path).toString());
        return Object.assign(Object.assign({}, data), attach);
    }
    catch (e) {
        echo_1.echo(`从文件中读取数据失败，检查路径是否正确或是否具备读写权限。错误信息：${e.message}`);
        return {};
    }
};
