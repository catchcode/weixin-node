import { WX } from '..';
export declare type buttonType = "click" | "view" | "scancode_push" | "scancode_waitmsg" | "pic_sysphoto" | "pic_photo_or_album" | "pic_weixin" | "location_select" | "media_id" | "view_limited";
export interface button {
    /** 菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型 */
    type: buttonType;
    /** 菜单标题，不超过16个字节，子菜单不超过60个字节 */
    name: string;
    /** 菜单KEY值，用于消息接口推送，不超过128字节 */
    key?: string;
    /** 网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时，不支持小程序的老版本客户端将打开本url。 */
    url?: string;
    /** 小程序的appid（仅认证公众号可配置） */
    appid?: string;
    /** 小程序的页面路径 */
    pagepath?: string;
    /** 调用新增永久素材接口返回的合法media_id */
    media_id?: string;
    /** 二级菜单数组，个数应为1~5个 */
    sub_button?: button[];
}
export interface subButton {
    /** 菜单标题，不超过16个字节，子菜单不超过60个字节 */
    name: string;
    sub_button: button[];
}
export declare class Menu {
    wx: WX;
    /** 创建菜单的请求地址 */
    readonly CREAT_URL = "https://api.weixin.qq.com/cgi-bin/menu/create";
    /** 获取菜单的请求地址 */
    readonly GET_URL = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info";
    /** 删除菜单的请求地址 */
    readonly DELETE_URL = "https://api.weixin.qq.com/cgi-bin/menu/delete";
    /** axios 实例 */
    private axios;
    constructor(wx: WX);
    /** 创建接口 */
    create(params: button | button[] | subButton | subButton[]): Promise<{
        errcode: number;
        errmsg: string;
    }>;
    /** 查询接口 */
    get_current_selfmenu_info(): Promise<{
        /** 菜单是否开启，0代表未开启，1代表开启 */
        is_menu_open: number;
        /** 菜单信息 */
        selfmenu_info: {
            readonly button: button[] & subButton[];
        };
    }>;
    /** 删除接口 */
    delete(): Promise<{
        errcode: number;
        errmsg: string;
    }>;
}
