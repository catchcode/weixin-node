/// <reference types="node" />
import { WX } from "..";
export declare type types = 'image' | 'voice' | 'video' | 'thumb';
interface article {
    /** 标题 */
    title: string;
    /** 图文消息的封面图片素材id（必须是永久mediaID） */
    thumb_media_id: string;
    /** 作者 */
    author?: string;
    /** 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空。如果本字段为没有填写，则默认抓取正文前54个字。 */
    digest?: string;
    /** 是否显示封面，0为false，即不显示，1为true，即显示 */
    show_cover_pic: string;
    /** 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS,涉及图片url必须来源 "上传图文消息内的图片获取URL"接口获取。外部图片url将被过滤。 */
    content: string;
    /** 图文消息的原文地址，即点击“阅读原文”后的URL */
    content_source_url: string;
    /** Uint32 是否打开评论，0不打开，1打开 */
    need_open_comment?: boolean;
    /** Uint32 是否粉丝才可评论，0所有人可评论，1粉丝才可评论 */
    only_fans_can_comment?: boolean;
}
interface news {
    "news_item": [
        {
            /** 图文消息的标题 */
            "title": string;
            /** 图文消息的封面图片素材id（必须是永久mediaID） */
            "thumb_media_id": number;
            /** 是否显示封面，0为false，即不显示，1为true，即显示 */
            "show_cover_pic": 1 | 0;
            /** 作者 */
            "author": string;
            /** 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空 */
            "digest": string;
            /** 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS */
            "content": string;
            /** 图文页的URL */
            "url": string;
            /** 图文消息的原文地址，即点击“阅读原文”后的URL */
            "content_source_url": string;
        }
    ];
}
interface video {
    "title": string;
    "description": string;
    "down_url": string;
}
export declare class Material {
    wx: WX;
    private axios;
    private ADD_NEWS;
    private UPLOADIMG;
    private ADD_MATERIAL;
    private GET_MATERIAL;
    private DEL_MATERIAL;
    private UPDATE_NEWS;
    private GET_MATERIALCOUNT;
    private BATCHGET_MATERIAL;
    constructor(wx: WX);
    /** 新增永久图文素材 */
    addNews(articles: article[] | article): Promise<import("axios").AxiosResponse<any>>;
    /**
     * 上传图文消息内的图片获取URL
     * @description 本接口所上传的图片不占用公众号的素材库中图片数量的100000个的限制。图片仅支持jpg/png格式，大小必须在1MB以下。
     */
    uploadImg(file: string): Promise<any>;
    /**
     * 新增其他类型永久素材
     * 上传视频请使用 addMaterialVideo 方法
     */
    addMaterial(file: string, type?: types): Promise<any>;
    /**
     *
     * @param file
     * @param description
     * @returns
     */
    addMaterialVideo(file: string, description: {
        /** 视频素材的标题 */
        title: string;
        /** 视频素材的描述 */
        introduction: string;
    }): Promise<{
        media_id: number; /** 图文消息的封面图片素材id（必须是永久mediaID） */
        url: string;
    }>;
    /**
     * 获取永久素材
     * @param media_id 要获取的素材的media_id
     */
    getMaterial(media_id: string): Promise<news & video & Buffer>;
    /**
     * 删除永久素材
     * @param media_id
     * @description 在新增了永久素材后，开发者可以根据本接口来删除不再需要的永久素材，节省空间。
     */
    delMaterial(media_id: string): Promise<{
        "errcode": 0;
        "errmsg": 'ok';
    }>;
    /**
     * 修改永久图文素材
     * @param news 参数
     * @returns
     */
    update_news(news: {
        media_id: string;
        index: number;
        articles: article;
    }): Promise<import("axios").AxiosResponse<any>>;
    /**
     * 获取素材总数
     * @returns
     */
    getMaterialcount(): Promise<{
        "voice_count": number;
        "video_count": number;
        "image_count": number;
        "news_count": number;
    }>;
    /**
     * 获取素材列表
     * @param params
     * @returns
     * @see https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/Get_materials_list.html
     */
    batchgetMaterial(params: {
        /** 素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news） */
        "type": types;
        /** 从全部素材的该偏移位置开始返回，0表示从第一个素材 返回 */
        "offset": number;
        /** 返回素材的数量，取值在1到20之间 */
        "count": number;
    }): Promise<{
        total_count: number;
        item_count: number;
        item: [
            {
                media_id: string;
                update_time: string;
                [key: string]: any;
            }
        ];
    }>;
}
export {};
