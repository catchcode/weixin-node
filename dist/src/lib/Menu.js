"use strict";
/*
 * @Descripttion: 自定义菜单
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-11 20:37:11
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-02 21:36:15
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Menu = void 0;
const axios_1 = __importDefault(require("axios"));
const axios = axios_1.default.create();
const WXAxios_1 = require("../tool/WXAxios");
class Menu {
    constructor(wx) {
        this.wx = wx;
        /** 创建菜单的请求地址 */
        this.CREAT_URL = 'https://api.weixin.qq.com/cgi-bin/menu/create';
        /** 获取菜单的请求地址 */
        this.GET_URL = 'https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info';
        /** 删除菜单的请求地址 */
        this.DELETE_URL = 'https://api.weixin.qq.com/cgi-bin/menu/delete';
        this.axios = WXAxios_1.create_axios(wx);
    }
    /** 创建接口 */
    create(params) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!(params instanceof Array)) {
                params = [params];
            }
            return this.axios.post(this.CREAT_URL, { button: params });
        });
    }
    /** 查询接口 */
    get_current_selfmenu_info() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.get(this.GET_URL);
        });
    }
    /** 删除接口 */
    delete() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.axios.get(this.DELETE_URL);
        });
    }
}
exports.Menu = Menu;
