import { toJSON, toXML } from '../src/tool/parser'
import assert from 'assert';

describe('JSON 和 XML 转换', function () {
  it('JSON to XML', function () {
    const xml = '<xml><ToUserName>toUser</ToUserName><FromUserName>fromUser</FromUserName><CreateTime>12345678</CreateTime><MsgType>text</MsgType><Content>你好</Content></xml>'
    const json = {
      ToUserName: 'toUser',
      FromUserName: 'fromUser',
      CreateTime: '12345678',
      MsgType: 'text',
      Content: '你好'
    }
    assert.deepStrictEqual(xml, toXML(json))
  })

  it('XML to JSON', function () {
    const xml = '<xml><ToUserName>toUser</ToUserName><FromUserName>fromUser</FromUserName><CreateTime>12345678</CreateTime><MsgType>text</MsgType><Content>你好</Content></xml>'
    const json = {
      ToUserName: 'toUser',
      FromUserName: 'fromUser',
      CreateTime: '12345678',
      MsgType: 'text',
      Content: '你好'
    }
    assert.deepStrictEqual(toJSON(xml), json)

  })
})