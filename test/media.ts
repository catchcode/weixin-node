/**
 * @name: 自由如风
 * @access: private
 * @version: 1.0
 * @param {*}
 * @return {*}
 * @description 临时素材单元测试
 */


import assert from 'assert';
import fs from 'fs'
import { WX } from '../src/index'
import path from 'path'
import { Downloader } from '../src/tool/Downloader';


const wx = new WX({
    token: 'codeon',
    port: 4005,
    appid: 'wxfa3f61d7c730edc9',
    secret: 'f7e8a648851ad0cdb62e2ab614d2d574'
})

describe("临时素材", function () {
    before(async () => {
        this.timeout(10 * 1000)
        await wx.accessToken?.wait();
    })


    it("普通文件下载", async function () {
        this.timeout(10 * 1000)
        try {
            fs.writeFileSync(path.resolve('test', 'get', 'plain_file.png'), await new Downloader('https://www.baidu.com/img/flexible/logo/pc/result.png').getBinary(), { encoding: 'binary' })
        } catch (e) {
            console.log(e);
        }
    })

    it("上传、获取图片", async function () {
        this.timeout(10 * 1000)
        const result = await wx.media.upload('image', fs.createReadStream(path.resolve('test', 'resource', 'image.jpg')))

        assert.strictEqual(result.type, 'image')
        assert.notStrictEqual(result.media_id, undefined)
        assert.notStrictEqual(result.created_at, undefined)
        await wx.media.getDownloader(result.media_id).save(path.resolve('test', 'get', 'image.jpg'))

    })

    it("上传、获取语音", async function () {
        this.timeout(10 * 1000)
        const result = await wx.media.upload('voice', fs.createReadStream(path.resolve('test', 'resource', 'voice.mp3')))

        assert.strictEqual(result.type, 'voice')
        assert.notStrictEqual(result.media_id, undefined)
        assert.notStrictEqual(result.created_at, undefined)
        await wx.media.getDownloader(result.media_id).save(path.resolve('test', 'get', 'voice.mp3'))

    })

    it("上传、获取视频", async function () {
        this.timeout(10 * 1000)
        const result = await wx.media.upload('video', fs.createReadStream(path.resolve('test', 'resource', 'video.mp4')))
        assert.strictEqual(result.type, 'video')
        assert.notStrictEqual(result.media_id, undefined)
        assert.notStrictEqual(result.created_at, undefined)
        const downloader = await wx.media.getDownloaderOfVideo(result.media_id)
        await downloader.save(path.resolve('test', 'get', 'video.mp4'))

    })

    it("上传、获取缩略图", async function () {
        this.timeout(10 * 1000)
        const result = await wx.media.upload('thumb', fs.createReadStream(path.resolve('test', 'resource', 'thumb.jpg')))

        assert.strictEqual(result.type, 'thumb')
        assert.notStrictEqual(result.thumb_media_id, undefined)
        assert.notStrictEqual(result.created_at, undefined)
        const data: any = await wx.media.getDownloader(result.thumb_media_id).save(path.resolve('test', 'get', 'thumb.jpg'))

    })

    it("获取失败")
})