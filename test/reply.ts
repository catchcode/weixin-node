/*
 * @Descripttion: 描述
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-16 16:56:35
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-17 20:50:57
 */
import assert from 'assert';
import { WX, Plugin } from '../src/index'
import { Reply } from '../src/lib/wxReply';
import { Request } from '../src/lib/wxRequest';
import { Imitate } from '../src/tool/Imitate';
import { toJSON } from '../src/tool/parser'
const imitate = new Imitate({ baseURL: 'http://localhost:4001' })
const wx = new WX({
    token: 'codeon',
    port: 4001
})


describe('被动回复用户消息', function () {
    before(() => {
        class SimpleText extends Plugin {

            test() {
                return this.req.is('text') && this.req.text.Content == 'SimpleText';
            }

            process() {
                return 'SimpleText'
            }
        }
        class Text extends Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'text';
            }
            process() {
                return this.reply.text('hello')
            }
        }

        class Image extends Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'image';
            }
            process() {
                return this.reply.image('MediaId')
            }
        }

        class Voice extends Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'voice';
            }
            process() {
                return this.reply.voice('MediaId')
            }
        }

        class Video extends Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'video';
            }
            process() {
                return this.reply.video({ MediaId: 'MediaId', Title: 'Title', Description: 'Description' })
            }
        }

        class Music extends Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'music';
            }
            process() {
                return this.reply.music({ Title: 'title', ThumbMediaId: 'ThumbMediaId', Description: 'Description', HQMusicUrl: 'HQMusicUrl', MusicUrl: 'MusicUrl' })
            }
        }

        class News extends Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'news';
            }
            process() {
                return this.reply.news([{ Description: 'Description', PicUrl: 'PicUrl', Title: 'Title', Url: 'Url' }])
            }
        }

        class MoreNews extends Plugin {
            test() {
                return this.req.is('text') && this.req.text.Content == 'morenews';
            }
            process() {
                const arr = [];
                for (let i = 0; i < 3; i++) {
                    arr.push({ Description: 'Description', PicUrl: 'PicUrl', Title: 'Title', Url: 'Url' })
                }
                return this.reply.news(arr)
            }
        }
        wx.use(Text).use(SimpleText)
        wx.use(Image)
        wx.use(Voice)
        wx.use(Video)
        wx.use(Music)
        wx.use(News)
        wx.use(MoreNews)
    })
    it("简单文本消息", async () => {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = await imitate.sendText(<any>{ Content: 'SimpleText' })

        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: Date.now().toString(),
            Content: 'SimpleText',
            MsgType: 'text'
        };
        result_2.CreateTime = res_json.CreateTime
        assert.deepStrictEqual(res_json, result_2);
    })
    it("默认文本回复", async () => {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = await imitate.sendText(<any>{ Content: 'defaultString' })
        assert.deepStrictEqual(res.data, 'success');
    })
    it("文本消息", async () => {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = await imitate.sendText(<any>{ Content: 'text' })
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: Date.now().toString(),
            Content: 'hello',
            MsgType: 'text'
        };
        result_2.CreateTime = res_json.CreateTime
        assert.deepStrictEqual(res_json, result_2);
    })

    it('图片消息', async () => {
        const res = await imitate.sendText(<any>{ Content: 'image' })

        const res_json = toJSON(res.data);

        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617424541503',
            MsgType: 'image',
            Image: { MediaId: 'MediaId' }
        };
        result_2.CreateTime = res_json.CreateTime
        assert.deepStrictEqual(res_json, result_2);
    })

    it('语音消息', async () => {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = await imitate.sendText(<any>{ Content: 'voice' })
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617426212775',
            MsgType: 'voice',
            Voice: { MediaId: 'MediaId' }
        };
        result_2.CreateTime = res_json.CreateTime
        assert.deepStrictEqual(res_json, result_2);
    })

    it('视频消息', async () => {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = await imitate.sendText(<any>{ Content: 'video' })
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617426212775',
            MsgType: 'video',
            Video: { MediaId: 'MediaId', Title: 'Title', Description: 'Description' }
        };
        result_2.CreateTime = res_json.CreateTime
        assert.deepStrictEqual(res_json, result_2);
    })

    it('音乐消息', async () => {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = await imitate.sendText(<any>{ Content: 'music' })
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617444601887',
            MsgType: 'music',
            Music: {
                Title: 'title',
                ThumbMediaId: 'ThumbMediaId',
                Description: 'Description',
                HQMusicUrl: 'HQMusicUrl',
                MusicUrl: 'MusicUrl'
            }
        };
        result_2.CreateTime = res_json.CreateTime
        assert.deepStrictEqual(res_json, result_2);
    })

    it("单图文消息", async () => {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = await imitate.sendText(<any>{ Content: 'news' })
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617445964668',
            MsgType: 'news',
            Articles: {
                item: {
                    Description: 'Description',
                    PicUrl: 'PicUrl',
                    Title: 'Title',
                    Url: 'Url'
                }
            },
            ArticleCount: '1'
        };
        result_2.CreateTime = res_json.CreateTime
        assert.deepStrictEqual(res_json, result_2);
    })

    it("多图文消息", async () => {
        // 模拟微信服务器向 模块发送消息，此时的 res 是微信服务器收到的结果
        const res = await imitate.sendText(<any>{ Content: 'morenews' })
        // 由于结果是 xml 所以转换成 json 进行断言
        const res_json = toJSON(res.data);
        const result_2 = {
            ToUserName: 'FromUserName',
            FromUserName: 'ToUserName',
            CreateTime: '1617445964668',
            MsgType: 'news',
            Articles: {
                item: [
                    {
                        Description: 'Description',
                        PicUrl: 'PicUrl',
                        Title: 'Title',
                        Url: 'Url'
                    },
                    {
                        Description: 'Description',
                        PicUrl: 'PicUrl',
                        Title: 'Title',
                        Url: 'Url'
                    },
                    {
                        Description: 'Description',
                        PicUrl: 'PicUrl',
                        Title: 'Title',
                        Url: 'Url'
                    }]
            },
            ArticleCount: '3'
        };
        result_2.CreateTime = res_json.CreateTime
        assert.deepStrictEqual(res_json, result_2);
    })
});