import assert from 'assert';
import { WX } from '../src/index';
import { Imitate, CutInType } from '../src/tool/Imitate';


const wx = new WX({
    token: 'codeon',
    port: 4000
})
const imitate = new Imitate({ baseURL: 'http://localhost:4000' })

describe('token认证', function () {

    it("成功的认证", () => {
        // 使用`codeon`签名的请求
        return imitate.cutIn(CutInType.usableParams).then(res => {
            // typeof res.data == 'number'
            assert.strictEqual(res.data === 1498141000469575451, true, '认证失败');
        })
    })

    it("失败的验证", () => {
        return imitate.cutIn(CutInType.disabledParams).then(res => {
            // typeof res.data == 'number'
            assert.strictEqual(res.data, false, '认证失败');
        })
    })


});