/*
 * @Descripttion: 测试插件的各项功能是否正常
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-16 16:56:49
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-06-12 07:49:05
 */
import assert from 'assert';
import { Plugin } from "../src"

describe("验证插件各项功能是否正常", function () {
    it("基本状态", () => {
        class plugin extends Plugin {

        }
        assert.strictEqual(typeof plugin.description, 'string');
        assert.strictEqual(plugin.disable, false);
        assert.strictEqual(typeof plugin.priority, 'number');
        // @ts-ignore
        const p1 = new plugin(null);
        // @ts-ignore
        assert.strictEqual(p1.test(null), true);
        // @ts-ignore
        assert.strictEqual(p1.process(null), undefined);
    })

    it("处理文本")
    it("处理...")
})
