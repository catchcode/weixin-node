import assert from 'assert';
import { WX } from '../src/index'



describe("访问令牌", function () {
    it("成功获取", async function () {
        const wx = new WX({
            token: 'codeon',
            port: 4002,
            appid: 'wxfa3f61d7c730edc9',
            secret: 'f7e8a648851ad0cdb62e2ab614d2d574'
        })
        
        assert.strictEqual(await wx.accessToken?.wait(), true)
    })

    it("失败获取", async function () {
        const wx = new WX({
            token: 'codeon',
            port: 4003,
            appid: 'wx', // appid 不对
            secret: 'f7e8a648851ad0cdb62e2ab614d2d574'
        })

        assert.strictEqual(await wx.accessToken?.wait(), false)
    })
})