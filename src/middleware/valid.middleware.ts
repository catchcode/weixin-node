import sha1 from 'sha1'
import Koa from 'koa'
import { echo } from '../tool/echo';
interface validData {
    /** 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。 */
    readonly signature: String,
    /** 时间戳 */
    readonly timestamp: string,
    /** 随机数 */
    readonly nonce: String
    /** 随机字符串 */
    readonly echostr: string
}
/**
 * 
 * @param obj 微信服务器传入的GET参数
 * @param token 令牌
 * @returns 验证成功返回 echoStr 失败返回 false
 */
function valid(obj: validData, token: string): string | false {
    try {
        var str = [obj.nonce, obj.timestamp, token].sort().join('')
        return sha1(str) == obj.signature ? obj.echostr.toString() : false
    } catch (e) {
        return false;
    }
}


export default function (token: string) {
    return async function (ctx: Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) {
        // @ts-ignore
        const data: validData = ctx.request.query
        if (ctx.method == 'GET' && data.echostr !== undefined) {
            ctx.body = valid(data, token);
            echo(`接入验证>${ctx.body}`)
        } else {
            await next();
        }
    }
}