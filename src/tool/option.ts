/*
 * @Descripttion: 创建WX实例需要的参数
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-12 10:12:56
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 13:22:38
 */
import path from 'path'

export interface InterfaceOption {
    token: string;
    /** 开发者ID  */
    appid?: string;
    /** 开发者密码(AppSecret)  */
    secret?: string;
    /** 监听的端口 */
    port?: number;
    /** 自动刷新 access token 的时间间隔,单位 秒 ,默认 7000*/
    access_token_interval?: number;
    /** 当没有插件处理的时候回复的内容，默认 success  */
    defaultContent?: string;
    /** 插件的目录 */
    plugin_path?: string;
    /** 仓库的目录 */
    store_path?: string
}

/**
 * 默认配置
 */
export const defaultOption = {
    port: 80,
    access_token_interval: 7000,
    defaultContent: 'success',
    /** 默认的插件目录 */
    plugin_path: path.join(process.cwd(), 'plugin'),
    /** 默认的仓库目录 */
    store_path: path.join(process.cwd(), 'store')
}
// 这个仓库必须在 defaultOption 下面引入，因为它里面使用了上面的配置
import { Store } from '../lib/Store'

/** 仓库配置 */
export const storeOption = new Store('env')

/**
 * @name: 自由如风
 * @access: private
 * @version: 1.0
 * @param {Option} option
 * @return {Option}
 * @description 合并配置,这是使用的理念的是 代码配置 > UI 配置 > 默认配置
 */
export function joinOption(option: InterfaceOption): InterfaceOption {
    return {
        ...defaultOption, ...storeOption.getAll(), ...option
    }
}