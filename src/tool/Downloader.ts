/*
 * @Descripttion: 下载器
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-06-12 06:58:06
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-02 17:03:59
 */

import download from 'download'
import fs from 'fs'

export class Downloader {
    private download
    /**
     * 
     * @param url 下载文件的路径，可以传string或者URL实例
     */
    constructor(url: string | URL) {
        this.download = download(url.toString())
    }
    /**
     * 获取文件的二进制数据
     * @returns 
     */
    getBinary() {
        return this.download
    }

    /**
     * 将文件保存到文件
     * @param file 保存的文件路径
     * @returns 
     */
    async save(file: string | URL) {
        return fs.writeFileSync(file, await this.download)
    }


}
