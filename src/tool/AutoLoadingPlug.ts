/*
 * @Descripttion: 自动加载插件
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-16 12:29:09
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-05 19:42:53
 */

import fs from 'fs'
import { WX } from '..';
import { echo } from './echo';
import _path from 'path'
import { defaultOption } from './option'

export default async function (path: string = defaultOption.plugin_path, wx: WX) {
    // 等待 accesstoken 获取成功之后再加载插件
    await wx.accessToken?.wait();

    // 判断目录是否存在，不存在则创建
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
        echo(`插件目录不存在，已创建[${path}]`)
    }
    // 打开这个目录
    const dirs = await fs.promises.opendir(path).catch(e => {
        console.error("打开插件目录失败，请检查 plugin_path 参数对应的目录是否正常。");
        process.exit();
    })

    for await (const dir of dirs) {
        try {
            // 不加载 非js 文件
            if (!/\.js/.test(dir.name)) { continue; }

            const plugin = require(_path.join(path, dir.name));
            wx.use(plugin)
            echo(`加载插件[${plugin.name}]成功`)
        } catch (e) {
            echo(`自动加载插件失败,文件名：${dir.name}`)
        }
    }
}