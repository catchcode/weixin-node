/*
 * @Descripttion: 由于微信上大量使用了 curl 命令来提交文件，这里对这种使用场景做一个封装，经过研究发现这个curl需要三个参数，上传的文件、上传的地址、access_token
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-02 13:44:18
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-03 17:38:12
 */
import { execSync, spawnSync } from 'child_process'
import econv from 'iconv-lite'

/**
 * 使用 curl 上传文件
 * @param file 需要上传的文件路径
 * @param url 上传的地址 可以携带查询参数(一般需要携带 access_token 参数)
 * @returns 返回url接口的响应结果
 * @description 由于微信很多接口都是响应json格式的，所以内部以使用JSON.parse转换了
 */
export function curlUpload(file: string, url: string | URL) {
    if (typeof url == 'string') {
        url = new URL(url)
    }

    const curl_str = `curl -F media=@${file} "${url.toString()}"`
    return JSON.parse(execSync(curl_str, { stdio: 'pipe', encoding: 'utf8' }).toString())
}

/**
 * 新增永久视频素材
 * @param video_file 视频文件的路径
 * @param url 视频文件的地址
 * @param description 视频文件的描述
 * @returns 
 */
export function curlUploadVideo(video_file: string, url: URL, description: {
    /** 视频素材的标题 */
    title: string,
    /** 视频素材的描述 */
    introduction: string
}): {
    /** 新增的永久素材的media_id */
    "media_id": number,
    /** 新增的图片素材的图片URL（仅新增图片素材时会返回该字段） */
    "url": string
} {
    const curl_str = `curl ${url.toString()}  -F media=@${video_file} -F description='${JSON.stringify(description)}'`
    return JSON.parse(execSync(curl_str, { stdio: 'pipe' }).toString())
}