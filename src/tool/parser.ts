/*
 * @Descripttion: JSON 和 XML 相互转换
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-13 15:40:53
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-04-13 17:26:53
 */

import parser from 'fast-xml-parser'
/** 将XML字符串转换成 JS 对象 */
export function toJSON(xml_str: string): any {
    var res = parser.parse(xml_str, { parseNodeValue: false }).xml
    return res

}

export function isXML(xml_str: string): boolean {
    return parser.validate(xml_str) === true
}

const j2xParser = new parser.j2xParser({})
/** 将 JSON 或者 JS对象转换成 XML 字符串 */
export function toXML(json_or_object: object): string {
    return j2xParser.parse({ xml: json_or_object })
}
