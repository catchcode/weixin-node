/** 打印控制台日志 */
export function echo(...message: string[]) {
    /** 单元测试时不输出 */
    if (process.env.NODE_ENV?.trim() == 'test') { return }
    console.log(`[${new Date().toLocaleTimeString()}]`, ...message, '\n')
}