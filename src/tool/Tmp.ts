/*
 * @Descripttion: 创建一个临时文件
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-02 16:09:09
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-03 14:04:12
 */

import os from 'os'
import fs from 'fs'
import path from 'path'



/**
 * 创建一个临时文件
 */
export class Tmp {
    /** 临时文件保存的路径 */
    static DIR_PATH = path.join(os.tmpdir(), 'weixin-nodejs')
    static randName(): string {
        return Date.now().toString() + Math.random().toString();
    }
    /** 临时文件的路径 */
    public path: string

    /**
     * 创建一个临时文件，如果你要销毁临时文件，请使用 disroy 方法销毁
     * 如果你在创建临时文件的时候指定了 file_name 参数，那么 prefile 参数将失效
     * 如果你指定的 file_name 已经存在，那么删除之前的文件
     * @param params 参数
     */
    constructor(params: {
        /** 文件名 */
        file_name?: string,
        /** 后缀 */
        prefix?: string,
    }) {
        if (params.file_name) {
            this.path = path.join(Tmp.DIR_PATH, params.file_name)
            // 如果之前存在，则删除之前的文件
            if (fs.existsSync(this.path)) {
                fs.rmSync(this.path);
            }
            const f = fs.openSync(this.path, 'w')
            fs.closeSync(f);
        } else {
            this.path = Tmp.randName() + (params.prefix ?? '')
        }
    }

    /** 销毁临时文件 */
    destroy() {
        return fs.rmSync(this.path)
    }

    /** 重命名文件 */
    rename(new_name: string) {
        /** 之前的名字 */
        const old_path = this.path;
        /** 新的名字 */
        const new_path = path.join(old_path, '..', new_name);
        // 重命名
        fs.renameSync(old_path, new_path)
        // 更换自己的路径
        this.path = new_path
    }
}

// 如果目录不存在 则创建这个目录
if (!fs.existsSync(Tmp.DIR_PATH)) {
    fs.mkdirSync(Tmp.DIR_PATH)
}