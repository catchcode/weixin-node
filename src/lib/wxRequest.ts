/*
 * @Descripttion: 微信推送的消息
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-11 21:10:12
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-23 13:47:45
 */
export type MsgTypes = "text" | "image" | "voice" | "video" | "shortvideo" | "location" | "link" | 'event'
export interface Basic {
    /** 开发者微信号 */
    readonly ToUserName: string,
    /** 发送方帐号（一个OpenID） */
    readonly FromUserName: string,
    /* 消息创建时间 （整型）*/
    readonly CreateTime: string,
    /** 消息类型*/
    readonly MsgType: MsgTypes,
    /** 消息id，64位整型 */
    readonly MsgId: string,
}

export interface Text extends Basic {
    /** 文本消息内容 */
    readonly Content: string,
}

export interface Image extends Basic {
    /** 图片链接（由系统生成） */
    readonly PicUrl: string,
    /** 图片消息媒体id，可以调用获取临时素材接口拉取数据。 */
    readonly MediaId: string,
}

export interface Voice extends Basic {
    /** 语音消息媒体id，可以调用获取临时素材接口拉取该媒体 */
    readonly MediaId: string,
    /** 语音格式：amr */
    readonly Format: string,
    /** 语音识别结果，UTF8编码 */
    readonly Recognition?: string,
}

export interface Video extends Basic {
    /** 视频消息媒体id，可以调用获取临时素材接口拉取数据。 */
    readonly MediaId: string,
    /** 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。 */
    readonly ThumbMediaId: string,
}

export interface ShortVideo extends Basic {
    /** 视频消息媒体id，可以调用获取临时素材接口拉取数据。 */
    readonly MediaId: string,
    /** 视频消息缩略图的媒体id，可以调用获取临时素材接口拉取数据。 */
    readonly ThumbMediaId: string,
}

export interface Location extends Basic {
    /** 地理位置纬度 */
    readonly Location_X: string,
    /** 地理位置经度 */
    readonly Location_Y: string,
    /** 地图缩放大小 */
    readonly Scale: string,
    /** 地理位置信息 */
    readonly Label: string,
}

export interface Link extends Basic {
    /** 消息标题 */
    readonly Title: string,
    /** 消息描述 */
    readonly Description: string,
    /** 消息链接 */
    readonly Url: string,
}
import * as Event from './wxPushEvent'

export type All = Text & Image & Voice & Video & ShortVideo & Location & Link & Event.Click & Event.VIEW & Event.ScancodePush & Event.ScancodeWaitMsg & Event.PicSysPhoto & Event.PicPhotoOrAlbum & Event.PicWeixin & Event.LocationSelect & Event.ViewMiniProgram
export type Any = Text | Image | Voice | Video | ShortVideo | Location | Link | Event.Click | Event.VIEW | Event.ScancodePush | Event.ScancodeWaitMsg | Event.PicSysPhoto | Event.PicPhotoOrAlbum | Event.PicWeixin & Event.LocationSelect | Event.ViewMiniProgram


import { toJSON } from '../tool/parser';

/** 微信推送过来的数据 */
export class Request {
    /** 将 微信的 xml 转换成 json 后的数据 */
    public data: All;

    get text(): Text { return this.data }
    get image(): Image { return this.data }
    get voice(): Voice { return this.data }
    get shortVideo(): ShortVideo { return this.data }
    get location(): Location { return this.data }
    get link(): Link { return this.data }

    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} xml_str 微信推送过来的字符串（xml格式）
     */
    constructor(xml_str: string) {
        this.data = toJSON(xml_str)
    }
    /**
     * 判断推送过来的消息是不是某种类型
     * @param type 消息类型
     * @returns 
     */
    is(type: MsgTypes | MsgTypes[]): boolean {
        if (!(type instanceof Array)) {
            type = [type]
        }
        return type.indexOf(this.data.MsgType) != -1
    }

    /**
     * 判断推送过来的消息是不是某种事件类型
     * @param type 事件类型
     * @returns 
     * @description 内部已经做了对消息类型是 event 的判断，无需重复判断
     */
    isEvent(type: Event.EventTypes | Event.EventTypes[]): boolean {
        if (!(type instanceof Array)) {
            type = [type]
        }
        return this.is('event') && type.indexOf(this.data.Event) != -1
    }
}

