/*
 * @Descripttion: 用户标签管理
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-17 12:32:00
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-28 14:42:57
 */
import { WX } from '..';
import { create_axios } from '../tool/WXAxios';

interface responseResult {
    /** 错误码 */
    errcode: number,
    errmsg: string
}

export class Tag {
    private app: WX;

    // 标签管理
    private readonly CREATE_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/create';
    private readonly GET_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/get';
    private readonly UPDATE_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/update';
    private readonly DELETE_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/delete';
    private readonly GET_TAG_USER = 'https://api.weixin.qq.com/cgi-bin/user/tag/get';

    // 用户管理
    /** 批量为用户打标签 */
    private readonly BATCH_TAG_GING = 'https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging';
    /** 批量为用户取消标签 */
    private readonly BATCHUN_TAG_GING = 'https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging';
    /** 获取用户身上的标签列表 */
    private readonly GET_USER_TAG = 'https://api.weixin.qq.com/cgi-bin/tags/getidlist'

    private axios;
    constructor(wx: WX) {
        this.app = wx
        this.axios = create_axios(wx);
    }

    /**
     * 创建标签
     * @param name 标签名（30个字符以内）
     * @returns 
     */
    async createTag(name: string): Promise<{
        tag: {
            /** 标签id，由微信分配  */
            id: number,
            /** 标签名，UTF8编码 */
            name: string
        }
    }> {
        return this.axios.post(this.CREATE_TAG, { tag: { name } })
    }

    /**
     * 获取公众号已创建的标签
     * @returns 
     */
    async getTag(): Promise<{
        tags: {
            /** 标签id，由微信分配 */
            id: number,
            /** 标签名，UTF8编码 */
            name: string,
            /** 此标签下粉丝数 */
            count: number,
        }[]
    }> {
        return this.axios.get(this.GET_TAG)
    }

    /**
     * 编辑标签
     * @param update_tag 要编辑的标签
     * @returns 
     */
    async updateTag(params: {
        tag: {
            /** 标签id，由微信分配  */
            id: number,
            /** 标签名，UTF8编码 */
            name: string
        }
    }): Promise<responseResult> {
        return this.axios.post(this.UPDATE_TAG, params)
    }

    /**
     * 删除标签
     */
    async delTag(id: number): Promise<responseResult> {
        const data = { tag: { id } }
        return this.axios.post(this.DELETE_TAG, data)
    }

    /**
     * 获取标签下粉丝列表
     * @param tagid 标签ID
     * @param next_openid 第一个拉取的OPENID，不填默认从头开始拉取
     */
    async getTagUser(params: { tagid: number, next_openid?: string }): Promise<{
        /** 这次获取的粉丝数量  */
        count: number,
        data: {
            /** 粉丝列表 */
            openid: string[]
        },
        /** 拉取列表最后一个用户的openid */
        next_openid: string
    }> {
        return this.axios.post(this.GET_TAG_USER, params)
    }


    // ================================ 用户管理 ==========================================
    /**
     * 批量为用户打标签
     * @param openid_list 粉丝列表 
     * @param tagid 标签id
     * @returns 
     */
    async batchTagGing(params: { openid_list: string[], tagid: number }): Promise<responseResult> {
        return this.axios.post(this.BATCH_TAG_GING, params)
    }

    /**
     * 批量为用户取消标签
     * @param openid_list 粉丝列表 
     * @param tagid 标签id
     * @returns 
     */
    async batchunTagGing(params: { openid_list: string[], tagid: number }): Promise<responseResult> {
        return this.axios.post(this.BATCHUN_TAG_GING, params)
    }

    /**
     * 获取用户身上的标签列表
     * @param openid 用户 id
     * @returns 
     */
    async getUserTags(openid: string): Promise<{ tagid_list: number[] }> {
        return this.axios.post(this.GET_USER_TAG, { openid })
    }
}