/*
 * @Descripttion: 用户管理
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-17 10:21:18
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-02 21:36:23
 */

import { WX } from '..';
import { create_axios } from '../tool/WXAxios';


type langs = 'zh_CN' | 'zh_TW' | 'en' | string

interface responseInfo {
    /** 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。 */
    subscribe: number,
    /** 用户的标识，对当前公众号唯一 */
    openid: string,
    /** nickname */
    nickname: string,
    /** 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知 */
    sex: number,
    /** 用户的语言，简体中文为zh_CN */
    language: langs,
    /** 用户所在城市 */
    city: string,
    /** 用户所在省份 */
    province: string,
    /** 用户所在国家 */
    country: string,
    /** 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。 */
    headimgurl: string,
    /** 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间 */
    subscribe_time: number,
    /** 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。 */
    unionid: string,
    /** 公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注 */
    remark: string,
    /** 用户所在的分组ID（暂时兼容用户分组旧接口） */
    groupid: number,
    /** 用户被打上的标签ID列表 */
    tagid_list: number[],
    /** 返回用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION 公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，ADD_SCENE_QR_CODE 扫描二维码，ADD_SCENE_PROFILE_LINK 图文页内名称点击，ADD_SCENE_PROFILE_ITEM 图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_WECHAT_ADVERTISEMENT 微信广告，ADD_SCENE_OTHERS 其他 */
    subscribe_scene: string,
    /** 二维码扫码场景（开发者自定义） */
    qr_scene: number,
    /** 二维码扫码场景描述（开发者自定义） */
    qr_scene_str: string
}

/** 未关注的 */
interface responseInfo_NO {
    /** 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。 */
    subscribe: number,
    /** 用户的标识，对当前公众号唯一 */
    openid: string
}



export class User {
    private app: WX;
    /** 获取用户基本信息 */
    private readonly INFO = 'https://api.weixin.qq.com/cgi-bin/user/info';
    /** 批量获取用户基本信息 */
    private readonly BATCH_GET = 'https://api.weixin.qq.com/cgi-bin/user/info/batchget';


    /** 设置用户备注名 */
    private readonly UPDATE_REMAKE = 'https://api.weixin.qq.com/cgi-bin/user/info/updateremark';
    /** 批量获取 */
    private readonly GET_BATCH_USER = 'https://api.weixin.qq.com/cgi-bin/user/get'





    private axios;

    constructor(wx: WX) {
        this.app = wx;
        this.axios = create_axios(wx);
    }

    /**
     * 获取用户基本信息(UnionID机制)
     * @param openid 普通用户的标识，对当前公众号唯一
     * @param lang 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     * @returns 
     */
    async getInfo(openid: string, lang: langs = 'zh_CN'): Promise<responseInfo> {
        return this.axios.get(this.INFO, { params: { openid, lang, } })
    }

    /**
     * 批量获取用户基本信息
     * @param openid_array 用户的标识组成的数组，对当前公众号唯一
     * @param lang 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     * @description 开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条。
     */
    async getBatchInfo(openid_array: string[], lang: langs = 'zh_CN'): Promise<
        {
            user_info_list: responseInfo[] & responseInfo_NO[]
        }> {
        const user_list = openid_array.map(openid => {
            return {
                openid,
                lang
            }
        })
        const data = { user_list }
        return this.axios.post(this.BATCH_GET, data)
    }

    /**
     * 获取用户列表
     * @param next_openid 第一个拉取的OPENID，不填默认从头开始拉取
     * @returns 正确时返回JSON数据包
     * @description 公众号可通过本接口来获取帐号的关注者列表，关注者列表由一串OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
     */
    async getBatchUser(next_openid?: string): Promise<{
        total: number,
        count: number,
        data: {
            openid: string[]
        },
        next_openid: string
    }> {
        return this.axios.get(this.GET_BATCH_USER, { params: { next_openid } })
    }

    /**
     * 设置用户备注名
     * @param openid 用户标识
     * @param remark 新的备注名，长度必须小于30字符
     * @returns 
     */
    async setMark(openid: string, remark: string) {
        const data = { openid, remark }
        return this.axios.post(this.UPDATE_REMAKE, data)
    }

}