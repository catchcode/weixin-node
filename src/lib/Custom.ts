/*
 * @Descripttion: 客服消息
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-30 08:39:28
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-06-27 13:12:23
 */

import _axios from 'axios'
import { WX } from '../index'

import { create_axios } from '../tool/WXAxios'



type Msgtypes = 'text' | 'image' | 'voice' | 'video' | 'music' | 'news' | 'mpnews' | 'msgmenu';


class Custom {
    // 本地 axios 实例
    private axios;
    // 微信实例
    private app: WX;

    private readonly SendURL = 'https://api.weixin.qq.com/cgi-bin/message/custom/send';
    private readonly TypingURL = ' https://api.weixin.qq.com/cgi-bin/message/custom/typing'

    constructor(wx: WX) {
        this.app = wx;
        this.axios = create_axios(wx);
    }
    /** 客服接口-发消息 */
    async send(params: { msgtype: Msgtypes }): Promise<{ errcode: 0, errmsg: "ok" }> {
        return (await this.axios.post(this.SendURL, params))
    }

    /** 客服接口-发送文本消息 */
    async sendText(params: {
        /** 普通用户openid */
        touser: string,
        text: {
            /** 文本消息内容 */
            content: string,
        }
    }) {
        return this.send({ ...params, msgtype: 'text' });
    }
    /** 客服接口-发送图片消息 */
    async sendImage(params: {
        /** 普通用户openid */
        touser: string,
        image:
        {
            /** 图片的素材ID */
            media_id: string
        }
    }) {
        return this.send({ ...params, msgtype: 'image' });
    }

    


    /** 客服接口-发送语音消息 */
    async sendVoice(params: {
        /** 普通用户openid */
        touser: string,
        voice:
        {
            /** 语音的素材ID */
            media_id: string
        }
    }) {
        return this.send({ ...params, msgtype: 'voice' });
    }


    /** 客服接口-发送语音消息 */
    async sendVideo(params: {
        /** 普通用户openid */
        touser: string,
        video:
        {
            media_id: string,
            thumb_media_id: string,
            title: string,
            description: string
        }
    }) {
        return this.send({ ...params, msgtype: 'video' });
    }


    /** 客服接口-发送音乐消息 */
    async sendMusic(params: {
        /** 普通用户openid */
        touser: string,
        music:
        {
            title: string,
            description: string,
            musicurl: string,
            hqmusicurl: string,
            thumb_media_id: string,
        }
    }) {
        return this.send({ ...params, msgtype: 'music' });
    }

    /** 客服接口-发送图文消息（点击跳转到外链） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。 */
    async sendNews(params: {
        /** 普通用户openid */
        touser: string,
        news: {
            articles: [
                {
                    title: string,
                    description: string,
                    url: string,
                    picurl: string
                }
            ]
        }
    }) {
        return this.send({ ...params, msgtype: 'news' });
    }

    /** 客服接口-发送图文消息（点击跳转到外链） 图文消息条数限制在1条以内，注意，如果图文数超过1，则将会返回错误码45008。 */
    async sendNewsByMediaId(params: {
        /** 普通用户openid */
        touser: string,
        mpnews:
        {
            media_id: string
        }
    }) {
        return this.send({ ...params, msgtype: 'mpnews' });
    }

    /** 客服接口-发送菜单消息 */
    async sendMsgMenu(params: {
        /** 普通用户openid */
        touser: string,
        msgmenu: {
            head_content: string,
            list: [
                {
                    id: number,
                    content: string
                }
            ],
            tail_content: string,
        }
    }) {
        return this.send({ ...params, msgtype: 'msgmenu' });
    }

    /** 客服输入状态 */
    async typing(OPENID: string) {
        return this.axios.post(this.TypingURL, { touser: OPENID, "command": "Typing" })
    }
}

export default Custom