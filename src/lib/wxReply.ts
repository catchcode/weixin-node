/*
 * @Descripttion: 描述
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-04-13 12:55:29
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-19 13:13:36
 */
export enum MsgTypes {
    /** 回复文本消息 */
    text = "text",
    /** 回复图片消息 */
    image = "image",
    /** 回复语音消息 */
    voice = "voice",
    /** 回复视频消息 */
    video = "video",
    /** 回复音乐消息 */
    music = "music",
    /** 回复图文消息 */
    news = "news",
}
export interface Basic {
    /** 开发者微信号 */
    ToUserName?: string,
    /** 发送方帐号（一个OpenID） */
    FromUserName?: string,
    /* 消息创建时间 （整型）*/
    CreateTime?: string,
    /** 消息类型*/
    MsgType?: string,
}

export interface Text extends Basic {
    Content: String,
    MsgType: MsgTypes;
}


export interface Image extends Basic {
    Image: {
        /** 通过素材管理中的接口上传多媒体文件，得到的id。 */
        MediaId: string,
    }
}

export interface Voice extends Basic {
    Voice: {
        /** 通过素材管理中的接口上传多媒体文件，得到的id */
        MediaId: string,
    }
}

interface VideoItem {
    /** 通过素材管理中的接口上传多媒体文件，得到的id */
    MediaId: string;
    /** 视频消息的标题 */
    Title: string;
    /** 视频消息的描述 */
    Description: string;
}

export interface Video extends Basic {
    Video: VideoItem
}

interface MusicItem {
    /** 音乐标题 */
    Title?: string;
    /** 音乐描述 */
    Description?: string;
    /** 音乐链接 */
    MusicUrl?: string;
    /** 高质量音乐链接，WIFI环境优先使用该链接播放音乐 */
    HQMusicUrl?: string;
    /** 缩略图的媒体id，通过素材管理中的接口上传多媒体文件，得到的id */
    ThumbMediaId: string;
}

export interface Music extends Basic {
    Music: MusicItem
}


interface ArticlesItem {
    /** 图文消息标题 */
    Title: string;
    /** 图文消息描述 */
    Description: string;
    /** 图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200 */
    PicUrl: string;
    /** 点击图文消息跳转链接 */
    Url: string;
}

export interface News extends Basic {
    /** 图文消息个数；当用户发送文本、图片、语音、视频、图文、地理位置这六种消息时，开发者只能回复1条图文消息；其余场景最多可回复8条图文消息 */
    ArticleCount: string;
    /** 图文数组 */
    Articles: { item: ArticlesItem[] }
}



export type All = Text & Image & Voice & Video & Music & Video & News
export type Any = Text | Image | Voice | Video | Music | Video | News


export class Reply {
    public data: object = {};
    constructor(
        /* 开发者微信号 */
        public FromUserName: string,
        /* 接收方帐号（收到的OpenID） */
        public ToUserName: string,
        /** 消息创建时间 （整型） */
        public CreateTime: string = Date.now().toString()) {
        /** 回复的数据（JSON） */
        this.data = { FromUserName, ToUserName, CreateTime }
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} Content 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
     * @return {string} 返回文本格式的xml字符串
     * @description 回复文本消息
     */
    text(Content: string): Text {
        return { ...this.data, Content, MsgType: MsgTypes.text }
    }
    /**
     * @description 回复图片消息
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} MediaId 通过素材管理中的接口上传多媒体文件，得到的id。
     * @return {string} 返回图片格式的xml字符串
     */
    image(MediaId: string): Image {
        return { ...this.data, Image: { MediaId }, MsgType: MsgTypes.image }
    }
    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {string} MediaId
     * @return {string} 返回语音格式的xml字符串
     * @description 回复语音消息
     */
    voice(MediaId: string): Voice {
        return { ...this.data, Voice: { MediaId }, MsgType: MsgTypes.voice }
    }

    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {Video} video 
     * @return {string} 返回视频格式的xml字符串
     * @description 回复视频消息
     */
    video(Video: VideoItem): Video {
        return { ...this.data, Video, MsgType: MsgTypes.video }
    }

    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {MusicItem} option
     * @return {string} 返回音乐格式的xml字符串
     * @description 回复音乐
     */
    music(Music: MusicItem): Music {
        return {
            ...this.data,
            Music,
            MsgType: MsgTypes.music
        }
    }

    /**
     * @name: 自由如风
     * @access: private
     * @version: 1.0
     * @param {Articles} item
     * @return {string} 返回图文乐式的xml字符串
     * @description 回复图文
     */
    news(item: ArticlesItem[] | ArticlesItem): News {
        if (!(item instanceof Array)) {
            item = [item]
        }
        return {
            ...this.data,
            Articles: {
                item
            },
            ArticleCount: String(item.length),
            MsgType: MsgTypes.news
        }
    }

    /**
     * 进行拦截，不在继续执行后续插件【未写】
     */
    intercept() {

    }
}
