import Router from 'koa-router'

import Koa from 'koa'

import { api } from './api/index'



/**
 * 路由
 * 其处理优先级大于插件
 */
export const router = new Router();





/**
 * 验证项目是否可以正常被访问
 */
router.get('/', (ctx, next) => {
    ctx.body = '<h1">你的项目正常运行</h1>'
})

/**
 * 对外开放的API接口
 */
router.use(api.routes(), api.allowedMethods())
