/*
 * @Descripttion: 完成API的认证
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-06-25 11:28:59
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 13:33:05
 */
import Koa from 'koa'
import { ResponseError } from '../lib/ResponseError';
/**
 * 验证API请求是否合法
 * @param ctx 
 * @param next 
 */
export default async function (ctx: any | Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) {
    function getAuthorization() {
        const base64 = Buffer.from('weixin-nodejs' + ":" + 'weixin-nodejs').toString('base64');
        return 'Basic ' + base64;
    }

    if (ctx.get('Authorization') === getAuthorization()) {
        await next();
    } else {
        ctx.status = 401
        ctx.body = new ResponseError('认证失败', 401)
    }
}