/*
 * @Descripttion: 处理插件的响应内容，负责检测 ResponseError 异常和格式化输出
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-06-25 11:38:14
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:10:34
 */

import Koa from 'koa'
import { WXResponceError } from '../../tool/WXAxios'
import { ResponseError } from '../lib/ResponseError'

export default async function (ctx: any | Koa.ParameterizedContext<Koa.DefaultState, Koa.DefaultContext, any>, next: Function) {
    try {
        // 尽量使用 ctx.body 返回数据
        ctx.body = {
            msg: 'ok',
            code: 200,
            // 优先使用返回值
            data: await next() || ctx.body,
        }
    } catch (e) {
        ctx.status = 500
        if (e instanceof ResponseError) { // 手动在中间件中抛出错误
            ctx.body = {
                ...e.toJSON(),
                data: ctx.body,
            }
        } else if (e instanceof WXResponceError) { // 微信API接口报错(主要是在 axios 的拦截器里抛出的)
            ctx.body = {
                msg: e.message,
                code: e.code,
            }
        } else if (e instanceof Error) { // Error 作为父类，应该放在最后
            ctx.body = {
                msg: e.message,
                code: 500,
                data: ctx.body,
            }
        } else {
            console.error(e)
            ctx.body = {
                msg: "!!服务器异常!!",
                code: 500,
                data: ctx.body,
            }
        }
    } finally {
        ctx.body = JSON.stringify(ctx.body);
    }
}