/*
 * @Descripttion: 上传素材的时候使用的中间件
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-03 12:45:03
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-03 12:58:29
 */
import multer from '@koa/multer'
import os from 'os'
import path from 'path'

/**
 * 处理上传素材的中间件
 * @see https://blog.csdn.net/qq_42778001/article/details/104442163
 */
export const mediaFile = multer().single('media')