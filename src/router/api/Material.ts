/*
 * @Descripttion: 永久素材管理
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-02 13:23:38
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-05 12:25:40
 */
import Router from 'koa-router'
import { types } from '../../lib/Material';
import { Tmp } from '../../tool/Tmp';
import fs from 'fs'
import { mediaFile } from '../apimiddleware/mediaFile'
export const material = new Router({ prefix: '/material' });


/**
 * 获取用户列表
 */
material.post('/getBatchUser', function (ctx, next) {
    return ctx.wx.user.getBatchUser(ctx.request.query.next_openid as string)
})

/**
 * 获取素材列表
 */
material.post('/batchgetMaterial', function (ctx, next) {
    const params = ctx.request.body as {
        type: types;
        offset: number;
        count: number;
    }

    return ctx.wx.material.batchgetMaterial(params)
})

/** 获取素材[不能正常使用的接口] */
material.post('/getMaterial', function (ctx, next) {
    const media_id = ctx.request.query.media_id as string;
    return ctx.wx.material.getMaterial(media_id)
})

/**
 * 删除素材
 */
material.delete('/delMaterial', function (ctx, next) {
    const { media_id } = ctx.request.query as { media_id: string }
    return ctx.wx.material.delMaterial(media_id)
})


/**
 * 上传素材图片或者音频
 */
material.post(['/addMaterialImage', '/addMaterialVoice', '/addMaterialVideo'], mediaFile, async function (ctx, next) {
    // 获取上传的文件
    const file = ctx.request.file
    console.log(file);

    // 创建一个临时文件
    const tmp = new Tmp({ file_name: file.originalname })

    // 将上传的文件写入到临时文件中
    fs.writeFileSync(tmp.path, file.buffer)

    // 从请求路径中判断上传的类型，可能是 image 、 voice 、 video
    const type = ctx.path.substr(ctx.path.length - 5, 5).toLocaleLowerCase() as types;

    if (type == 'image' || type == 'voice') {
        // 将临时文件上传到微信服务器里
        var res = await ctx.wx.material.addMaterial(tmp.path, type)
    } else if (type == 'video') {
        // const res = await ctx.wx.material.addMaterialVideo(tmp.path,)
    }

    // 临时文件使用完毕后删除
    tmp.destroy();
    // 返回数据
    ctx.body = res
})

material.post('/addMaterialNews', function (ctx, next) {
    return ctx.wx.material.addNews(ctx.request.body)
})
