/*
 * @Descripttion: 主要是用户操作方面的API
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-02 22:53:44
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:33:20
 */

import Router from 'koa-router'

export const user = new Router({ prefix: '/user' })

/** 获取用户信息 */
user.get('/getUserInfo', function (ctx) {
    const { openId } = ctx.request.query as { openId: string }
    return ctx.wx.user.getInfo(openId)
})

/** 获取自己信息 */
user.get('/getSelfInfo', function (ctx) {
    ctx.body = ctx.wx.user.getInfo(ctx.wx.option.appid as string)
})

/** 获取用户列表 */
user.get('/getUserList', async function (ctx) {
    ctx.body = await ctx.wx.user.getBatchUser()
})

/** 批量获取用户信息 */
user.post('/getBatchInfo', async function (ctx) {
    const openid_array = ctx.request.body as string[]
    ctx.body = await ctx.wx.user.getBatchInfo(openid_array)
})
