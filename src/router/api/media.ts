/*
 * @Descripttion: 素材管理
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-06-25 13:22:16
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:10:47
 */
import Router from 'koa-router'

export const media = new Router({ prefix: '/media' });

media.get('/getMaterialcount', async function (ctx) {
    ctx.body = await ctx.wx.material.getMaterialcount()
})