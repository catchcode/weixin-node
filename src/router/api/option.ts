/*
 * @Descripttion: 负责操作自己的配置文件
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-05 19:01:31
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:31:56
 */
import Router from 'koa-router'

import { storeOption, joinOption } from '../../tool/option'

export const option = new Router({ prefix: '/option' });

option.put('/', async function (ctx, next) {
    storeOption.join(ctx.request.body).save()
    // 为了使配置生效，重新合并配置
    ctx.wx.option = joinOption(ctx.wx.codeOption);
    ctx.body = storeOption.getAll()
})

option.get('/', async function (ctx) {
    ctx.body = storeOption.getAll()
})


