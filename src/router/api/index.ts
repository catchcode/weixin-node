/*
 * @Descripttion: 对外暴露的 API 接口，方便其他程序调用
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-17 09:43:11
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:09:56
 */

import Router from 'koa-router'

import auth from '../apimiddleware/auth'
import response from '../apimiddleware/response'
import { ResponseError } from '../lib/ResponseError'
import { media } from './media'
import { material } from './Material'
import { system } from './system'
import { option } from './option'
import { user } from './user'
/**
 * 对外暴露的 API 接口，方便其他程序调用
 * @description 调用路径前面需要加上/api
 */
export const api = new Router({ prefix: '/api' });

/**
 * 所有的接口都应用这个认证中间件
 */
api.use(auth)
/**
 * 格式化输出
 */
api.use(response)


/** 前台将通过这个路由测试 ping */
api.get('ping', '/', function (ctx, next) {
    ctx.body = '与服务器通信正常';
})

api.get('/error', function (ctx) {
    throw new ResponseError('抛出错误', 123)
})

/** 永久素材管理 */
api.use(material.routes(), material.allowedMethods())
/** 临时素材管理 */
api.use(media.routes(), media.allowedMethods())
/** 系统级API */
api.use(system.routes(), system.allowedMethods())
/** 参数配置 */
api.use(option.routes(), option.allowedMethods())
/** 用户管理 */
api.use(user.routes(), user.allowedMethods())