/*
 * @Descripttion: 浏览器发送一个描述被执行的代码对象，服务器解析这个对象并执行
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-07-05 10:11:14
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-07-06 10:11:09
 */

import Router from 'koa-router'
import { WX } from '../../index'
import { ResponseError } from '../lib/ResponseError';
import fs from 'fs'
import path from 'path'
import multer from '@koa/multer'

/** 客户端的请求类型 */
interface ClientRequest {
    arguments: any[],
    function: (...[]) => any;
    app: WX,
}
export const system = new Router({ prefix: '/system' })

// JSON 的转换器，只抓换这个对象键名为 function 的键，将其还原成一个函数
function parse(key: string, value: any) {
    if (key == 'function') {
        return eval(value)
    }
    return value;
}

/** 直接执行前端发送过来的代码 */
system.post('/', function (ctx, next) {
    // 将传过来的数据转换成可被执行的对象
    const clientRequest = JSON.parse(ctx.request.body, parse) as ClientRequest
    // 将自己的app挂载上去，使其被调用
    clientRequest.app = ctx.wx;
    // 执行，并将结果返回
    ctx.body = clientRequest.function(...clientRequest.arguments)
})

/** 获取插件的信息 */
system.get('/pluginsInfo', function (ctx) {


    ctx.body = ctx.wx.plugins.map(function (e) {
        return {
            // 插件的描述
            description: e.description,
            // 插件的名字
            name: e.name,
            // 插件的开关状态
            disable: e.disable,
        }
    })
    console.log(ctx.wx.plugins[0]);
})

/** 设置插件的状态 */
system.put('/setPluginState', function (ctx) {
    const { disable, name } = ctx.request.body as { disable: boolean, name: string }

    const plugin = ctx.wx.plugins.find(e => e.name == name)
    if (plugin) {
        plugin.disable = Boolean(disable);
    } else {
        throw new ResponseError('插件不存在', 3366)
    }
    ctx.body = '修改成功'
})

/** 添加一个插件 */
system.post('/addPlugin', multer().single('script'), function (ctx) {

    const buffer = ctx.request.file.buffer
    const file_name = ctx.request.file.originalname
    const plugin_path = ctx.wx.option.plugin_path as string
    const file_path = path.join(plugin_path, file_name)
    fs.writeFileSync(file_path, buffer)
    const plugin = require(file_path)
    ctx.wx.use(plugin);
    ctx.body = '成功添加'
})
