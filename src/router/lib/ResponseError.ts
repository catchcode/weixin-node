/*
 * @Descripttion: 所有的API都应该抛出这个异常
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-06-25 11:54:18
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-06-25 11:55:03
 */
export class ResponseError extends Error {
    constructor(public message: string, public code: number) {
        super(message)
    }
    toString() {
        return JSON.stringify(this.toJSON())
    }

    toJSON() {
        return {
            message: this.message,
            code: this.code
        }
    }
}